# React-Sidebar

This is a simple application intended to demonstrate my skills in frontend design using React and Redux. The tables, forms and charts have been created using devextreme components, all other work is original however.

![](/screenshot.png)