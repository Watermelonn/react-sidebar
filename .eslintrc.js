module.exports = {
    "parser": "babel-eslint",
    "parserOptions": {
        "parser": "babel-eslint",
        "sourceType": "module",
        "allowImportExportEverywhere": true
    }
};