import "react-app-polyfill/ie11";
import React from "react";
import ReactDOM from "react-dom";
import { HashRouter } from "react-router-dom";
import { configureStore } from "redux-starter-kit";
import { Provider } from "react-redux";
import { CookiesProvider } from "react-cookie";
import * as serviceWorker from "./serviceWorker";
import App from "./App";
import navigationReducer from "./reducers/navigationReducer";
import popupsReducer from "./reducers/popupsReducer";
import themeReducer from "./reducers/themeReducer";
import operativeDtoReducer from "./reducers/operatives/dtos/operativeDtoReducer";
import manageOperativesReducer from "./reducers/operatives/manageOperativesReducer";
import administrationReducer from "./reducers/administration/administrationReducer";
import manageContractorsReducer from "./reducers/contractor/manageContractorsReducer";
import favouriteReportsReducer from "./reducers/report/favouriteReportsReducer";
import "bootstrap/dist/css/bootstrap.min.css";
import "devextreme/dist/css/dx.common.css";
import "./styles/siteCommon.css";
import config from "devextreme/core/config";

config({
  forceIsoDateParsing: true
});

const store = configureStore({
  reducer: {
    navigation: navigationReducer,
    popups: popupsReducer,
    theme: themeReducer,
    operativeDto: operativeDtoReducer,
    manageOperatives: manageOperativesReducer,
    administration: administrationReducer,
    manageContractors: manageContractorsReducer,
    favouriteReports: favouriteReportsReducer
  }
});

const render = Component => {
  ReactDOM.render(
    <CookiesProvider>
      <Provider store={store}>
        <HashRouter>
          <Component />
        </HashRouter>
      </Provider>
    </CookiesProvider>,
    document.getElementById("root")
  );
};

window.onload = async () => {
  render(App);
};

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
