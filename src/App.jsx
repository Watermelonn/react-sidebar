import React, { Component } from "react";
import { connect } from "react-redux";
import { withCookies } from "react-cookie";
import Sidebar from "./components/navigation/sidebar/sidebar";
import Router from "./components/router";
import "./styles/siteTheme.css";
import "devextreme/dist/css/dx.common.css";
import "./styles/popups.css";

class App extends Component {
  async componentDidMount() {
    const { cookies, toggleTheme } = this.props;

    const darkTheme = cookies.cookies.darkTheme;

    if (darkTheme === "true") {
      toggleTheme();
    }
  }

  render() {
    const { cookies } = this.props;

    return (
      <div className="page-container">
        <Sidebar cookies={cookies} />
        <Router cookies={cookies} />
      </div>
    );
  }
}

const mapStateToProps = state => {
  const { theme } = state;
  return {
    darkTheme: theme.darkTheme
  };
};

const mapDispatchToProps = dispatch => {
  return {
    toggleTheme: () => {
      dispatch({ type: "TOGGLE_THEME" });
    }
  };
};
export default withCookies(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(App)
);
