import { createReducer } from "redux-starter-kit";

const initState = {
  manageContractorsTable: {
    buttonsDisabled: true,
    showInactive: false
  },
  selectedContractorId: null,
  refresh: false
};

const manageContractorsReducer = createReducer(initState, {
  ENABLE_MANAGE_CONTRACTORS_TABLE_BUTTONS: state => {
    state.manageContractorsTable.buttonsDisabled = false;
  },
  CHANGE_SELECTED_CONTRACTOR: (state, action) => {
    state.selectedContractorId = action.id;
  },
  TOGGLE_MANAGE_CONTRACTORS_REFRESH: state => {
    state.refresh = !state.refresh;
  },
  TOGGLE_SHOW_INACTIVE_CONTRACTORS: state => {
    state.manageContractorsTable.showInactive = !state.manageContractorsTable
      .showInactive;
  }
});

export default manageContractorsReducer;
