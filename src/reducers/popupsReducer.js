import { createReducer } from "redux-starter-kit";

const initState = {
  editOperativePopup: {
    visible: false
  },
  userSettingsPopup: {
    visible: false
  },
  notesPopup: {
    visible: false
  },
  okCancelPopup: {
    visible: false
  },
  addEditCompanyContactPopup: {
    visible: false
  },
  viewSwipesPopup: {
    visible: false
  },
  manualSwipePopup: {
    visible: false
  },
  editPassNumberPopup: {
    visible: false
  },
  manageAccessPopup: {
    visible: false
  },
  printCardPopup: {
    visible: false
  },
  addTradePopup: {
    visible: false
  },
  weekendWorkPopup: {
    visible: false
  },
  addEditEmergencyOperativePopup: {
    visible: false
  },
  datePickerPopup: {
    visible: false,
    date: null
  },
  tradePopup: {
    visible: false
  },
  coreDisciplinePopup: {
    visible: false
  },
  departmentPopup: {
    visible: false
  },
  screeningTypePopup: {
    visible: false
  },
  contractorTypePopup: {
    visible: false
  },
  authorisorPopup: {
    visible: false
  },
  tradeDeletePopup: {
    visible: false
  },
  coreDisciplineDeletePopup: {
    visible: false
  },
  departmentDeletePopup: {
    visible: false
  },
  screeningTypeDeletePopup: {
    visible: false
  },
  contractorTypeDeletePopup: {
    visible: false
  },
  authorisorDeletePopup: {
    visible: false
  },
  logOutPopup: {
    visible: false
  },
  addEditLoginPopup: {
    visible: false
  },
  holdContractorPopup: {
    visible: false
  },
  auditTrailPopup: {
    visible: false
  },
  inductPopup: {
    visible: false
  },
  infoPopup: {
    visible: false
  },
  swipeOutPopup: {
    visible: false
  }
};

const popupsReducer = createReducer(initState, {
  TOGGLE_POPUP: (state, action) => {
    state[action.popup].visible = !state[action.popup].visible;
  },
  CHANGE_DATE_PICKER_DATE: (state, action) => {
    state.datePickerPopup.date = action.date;
  }
});

export default popupsReducer;
