import { createReducer } from "redux-starter-kit";

const initState = {
  trade: {
    buttonsDisabled: true,
    selectedId: null
  },
  coreDiscipline: {
    buttonsDisabled: true,
    selectedId: null
  },
  department: {
    buttonsDisabled: true,
    selectedId: null
  },
  screeningType: {
    buttonsDisabled: true,
    selectedId: null
  },
  contractorType: {
    buttonsDisabled: true,
    selectedId: null
  },
  authorisor: {
    buttonsDisabled: true,
    selectedId: null
  }
};

const administrationReducer = createReducer(initState, {
  ENABLE_ADMIN_TABLE_BUTTONS: (state, action) => {
    state[action.name].buttonsDisabled = false;
  },
  SET_ADMIN_SELECTED_ID: (state, action) => {
    state[action.name].selectedId = action.id;
  },
  CHANGE_ADMIN_INPUT_VALUE: (state, action) => {
    state[action.name][action.inputName] = action.value;
  },
  CLEAR_ADMIN_INPUT_VALUES: (state, action) => {
    state[action.name] = initState[action.name];
  }
});

export default administrationReducer;
