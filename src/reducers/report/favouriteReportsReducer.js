import { createReducer } from "redux-starter-kit";

const initState = {
  reports: []
};

const favouriteReportsReducer = createReducer(initState, {
  ADD_OR_REMOVE_FAVOURITE_REPORT: (state, action) => {
    if (state.reports.some(x => x === action.id)) {
      state.reports = state.reports.filter(x => x !== action.id);
    } else {
      state.reports.push(action.id);
    }
  },
  SET_FAVOURITE_REPORTS: (state, action) => {
    state.reports = action.reports;
  }
});

export default favouriteReportsReducer;
