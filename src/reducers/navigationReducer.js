import {
  sidebarPrimaryItems,
  sidebarSecondaryItems
} from "../constants/sidebarItems";
import { createReducer } from "redux-starter-kit";

const initState = {
  sidebarItems: {
    mainItems: sidebarPrimaryItems,
    secondaryItems: sidebarSecondaryItems
  },
  sidebarCollapsing: false,
  pageTitle: "React Example"
};

const navigationReducer = createReducer(initState, {
  TOGGLE_EXPAND_OPTION_LIST: (state, action) => {
    const mainItems = [...state.sidebarItems.mainItems];

    for (let mainItem of mainItems) {
      if (mainItem.id === action.id) {
        mainItem.closed = !mainItem.closed;
        continue;
      }
      mainItem.closed = true;
    }

    state.sidebarItems.mainItems = mainItems;
  },
  TOGGLE_SIDEBAR_OPTION_HOVER: (state, action) => {
    const mainItems = state.sidebarItems.mainItems;
    const index = mainItems.findIndex(x => x.id === action.id);

    mainItems[index][action.item] = action.value;
  },
  TOGGLE_SIDEBAR_OPTION_SUBLIST_HOVER: (state, action) => {
    const mainItems = state.sidebarItems.mainItems;
    const index = mainItems.findIndex(x => x.id === action.id);

    if (index === -1) return;

    const mainItem = mainItems[index];

    mainItem.allHover = mainItem.subHover || mainItem.mainHover;
  },
  TOGGLE_SIDEBAR_COLLAPSING: state => {
    state.sidebarCollapsing = !state.sidebarCollapsing;
  },
  SET_PAGE_TITLE: (state, action) => {
    state.pageTitle = action.title;
  }
});

export default navigationReducer;
