import { createReducer } from "redux-starter-kit";
import themes from "devextreme/ui/themes";

const initState = {
  darkTheme: false,
  themes: {
    dark: {
      "--background-colour": "#192231",
      "--main-colour": "#131c29",
      "--accent-colour": "#1c2636",
      "--text-colour": "#e1ebf5",
      "--border-colour": "#3b5175",
      "--line-colour": "#24344d",
      "--panel-colour": "#131c29",
      "--input-colour": "#192231"
    },
    light: {
      "--background-colour": "#f0f1f2",
      "--main-colour": "#fff",
      "--panel-colour": "#fff",
      "--accent-colour": "#fafafc",
      "--text-colour": "#3e3e3e",
      "--border-colour": "#ddd",
      "--line-colour": "#ddd",
      "--input-colour": "#fafafc"
    }
  }
};

const themeReducer = createReducer(initState, {
  TOGGLE_THEME: state => {
    state.darkTheme = !state.darkTheme;

    let currentTheme = state.darkTheme ? "dark" : "light";

    const theme = state.themes[currentTheme];

    Object.keys(theme).forEach(key => {
      const cssKey = key;
      const cssValue = theme[key];
      document.body.style.setProperty(cssKey, cssValue);
    });

    themes.current(`generic.${currentTheme}`);
  }
});

export default themeReducer;
