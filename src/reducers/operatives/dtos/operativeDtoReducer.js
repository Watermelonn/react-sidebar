import { createReducer } from "redux-starter-kit";
import moment from "moment";

const initState = {
  // operative details
  id: null,
  firstname: null,
  surname: null,
  contractor: null,
  contractorName: null,
  trade: null,
  tradeName: null,
  screeningRequired: false,
  screeningType: null,
  screeningDate: null,
  screeningExpiry: null,
  inductionDate: null,
  temporaryInduction: false,
  inductionExpiry: moment()
    .utc()
    .add(1, "years")
    .format("YYYY-MM-DDTHH:mm:ssZ"),
  comments: null,
  passNumber: null,
  operativeAccess: [],
  filename: null,
  operativePhotoUrl: null,

  // inputs
  tradeDescription: null,
  tradeDescriptionExists: false,

  panelId: null,
  swipeDateTime: null,
  reasonForSwipe: null,

  newPassNumber: null,
  newPassNumberExists: false,

  workType: null,
  workTypeContractor: null,
  companyContact: null,
  location: null,
  details: null,
  saturdayWork: null,
  sundayWork: null,
  saturdayContact: null,
  sundayContact: null,
  date: null,

  note: null
};

const operativeDtoReducer = createReducer(initState, {
  CHANGE_INPUT_VALUE: (state, action) => {
    state[action.name] = action.value;
  },
  CLEAR_FORM_INPUTS: state => {
    for (let property in initState) state[property] = initState[property];
  },
  CHANGE_ALL_INPUT_VALUES: (state, action) => {
    for (let property in action.operative) {
      state[property] = action.operative[property];
    }
  },
  CLEAR_WEEKEND_WORK_DETAILS: state => {
    state.workType = null;
    state.workTypeContractor = null;
    state.companyContact = null;
    state.location = null;
    state.details = null;
    state.saturdayWork = null;
    state.sundayWork = null;
    state.saturdayContact = null;
    state.sundayContact = null;
    state.date = null;
  }
});

export default operativeDtoReducer;
