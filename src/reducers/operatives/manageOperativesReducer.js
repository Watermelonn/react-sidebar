import { createReducer } from "redux-starter-kit";

const initState = {
  manageOperativesTable: {
    buttonsDisabled: true,
    showInactive: false
  },
  selectedOperativeId: null,
  currentLocation: null,
  refresh: false
};

const manageOperativesTable = createReducer(initState, {
  ENABLE_MANAGE_OPERATIVES_TABLE_BUTTONS: state => {
    state.manageOperativesTable.buttonsDisabled = false;
  },
  CHANGE_SELECTED_OPERATIVE: (state, action) => {
    state.selectedOperativeId = action.id;
  },
  CHANGE_OPERATIVE_CURRENT_LOCATION: (state, action) => {
    state.currentLocation = action.location;
  },
  TOGGLE_MANAGE_OPERATIVES_REFRESH: state => {
    state.refresh = !state.refresh;
  },
  TOGGLE_SHOW_INACTIVE_OPERATIVES: state => {
    state.manageOperativesTable.showInactive = !state.manageOperativesTable
      .showInactive;
  }
});

export default manageOperativesTable;
