import React, { Component } from "react";
import Menubar from "./navigation/menubar/menubar";
import { Switch, Route } from "react-router-dom";
import { connect } from "react-redux";
import AddEditOperative from "./operatives/addEditOperative";
import ManageOperative from "./operatives/manageOperatives";
import ManageContractors from "./contractors/manageContractors";
import ReportsMenu from "./reports/reportsMenu";
import HomeAdmin from "./home/homeAdmin";
import ControlPanel from "./administration/controlPanel";

class Router extends Component {
  handlePageAreaClicked = () => {
    const { sidebarCollapsed, cookies, toggleSidebarCollapsing } = this.props;

    if (sidebarCollapsed === "false" && window.innerWidth < 600) {
      cookies.set("sidebarCollapsed", true, { path: "/", expiry: "tomorrow" });
      toggleSidebarCollapsing();
    }
  };

  render() {
    const { cookies, sidebarCollapsed } = this.props;

    let collapsed = false;

    if (typeof sidebarCollapsed === "string") {
      collapsed = sidebarCollapsed === "true";
    } else {
      collapsed = sidebarCollapsed;
    }

    const pageAreaClassName = collapsed ? "collapsed" : "";

    return (
      <React.Fragment>
        <div
          className={"page-content " + pageAreaClassName}
          onTouchStart={() => this.handlePageAreaClicked()}
          onClick={() => this.handlePageAreaClicked()}
        >
          <Menubar onMenuToggle={this.handleMenuToggle} cookies={cookies} />
          <div className="page-detail">
            <Switch>
              <Route path="/operatives/addNew" component={AddEditOperative} />
              <Route path="/operatives/manage" component={ManageOperative} />
              <Route path="/contractors/manage" component={ManageContractors} />
              <Route path={"/reports/menu"} component={ReportsMenu} />
              <Route
                path="/administration/controlPanel"
                component={ControlPanel}
              />
              <Route exact path="/" component={HomeAdmin} />
            </Switch>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    cookies: ownProps.cookies,
    sidebarCollapsed: ownProps.cookies.cookies.sidebarCollapsed,
    sidebarCollapsing: state.navigation.sidebarCollapsing
  }
};

const mapDispatchToProps = dispatch => {
  return {
    toggleSidebarCollapsing: () => {
      dispatch({ type: "TOGGLE_SIDEBAR_COLLAPSING" });
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Router);
