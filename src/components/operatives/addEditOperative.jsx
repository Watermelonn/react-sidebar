import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Button,
  DateBox,
  SelectBox,
  TextArea,
  ValidationGroup
} from "devextreme-react";
import Validator, {
  CustomRule,
  RangeRule,
  RequiredRule,
  StringLengthRule
} from "devextreme-react/validator";
import notify from "devextreme/ui/notify";
import UniTextBox from "../common/forms/uniTextBox";
import UniSelectBox from "../common/forms/uniSelectBox";
import UniDatePicker from "../common/forms/uniDatePicker";
import UniPopup from "../common/popups/uniPopup";
import UniScrollView from "../common/forms/uniScrollView";
import UniCheckBox from "../common/forms/uniCheckBox";
import AddTrade from "./partials/addTrade";
import UniFileUploader from "../common/forms/uniFileUploader";
import SubmitButtonsGroup from "../common/forms/submitButtonsGroup";
import moment from "moment";
import { NumericRule } from "devextreme-react/tree-list";

class AddEditOperative extends Component {
  constructor(props) {
    super(props);
    this.validationGroup = React.createRef();
  }

  componentDidMount() {
    this.checkForEditMode();
  }

  componentDidUpdate() {
    this.checkForEditMode();
  }

  shouldComponentUpdate(nextProps) {
    const editMode = this.checkForEditMode();
    return !!(this.props.id !== nextProps.id || editMode);
  }

  getDisabledDays = args => {
    let date = new Date();
    date.setDate(date.getDate() - 1);

    return args.date <= date;
  };

  handleFormClear = () => {
    this.forceUpdate();
    this.props.clearFormInputs();
    this.validationGroup.current.instance.reset();
    this.forceUpdate();
  };

  handleChange = e => {
    const { changeInputValue } = this.props;

    let value = "";
    let name = "";

    if (e.value !== undefined) {
      value = e.value;
    } else if (e.event !== undefined) {
      value = e.event.target.value;
    } else if (e.target !== undefined) {
      value = e.target.checked;
    }

    if (e.element) {
      name = e.element.id;
      changeInputValue(name, value);
    } else {
      name = e.target.id;
      changeInputValue(name, value);
      this.forceUpdate();
    }
  };

  handleScreeningRequiredToggle = e => {
    this.handleChange(e);
    this.forceUpdate();
  };

  checkForEditMode = () => {
    const { location, history, changePageTitle } = this.props;
    let editMode = true;

    if (location) {
      const urlParams = new URLSearchParams(location.search);
      editMode = urlParams.get("editMode");
    }

    if (editMode === "false") {
      this.handleFormClear();
      history.push("/operatives/addNew");
      changePageTitle("Add New Operative");
      setTimeout(this.handleFormClear, 80);
    }

    return editMode;
  };

  handleShowPopup = popupName => {
    this.props.togglePopup(popupName);
  };

  handleFileUploaded = e => {
    const filename = e.request.response.substring(1).slice(0, -1);
    this.props.changeInputValue("filename", filename);
  };

  updateOperativeFile = async e => {};

  handleFormSubmit = async e => {
    e.preventDefault();

    const { firstname, surname } = this.props;

    notify(
      `Operative '${firstname} ${surname}' Added with Id 123.`,
      "success",
      1500
    );

    this.handleFormClear();
  };

  handleTemporaryInductionChanged = e => {
    const { changeInputValue } = this.props;
    this.handleChange(e);

    const expiryDate = e.target.checked
      ? moment()
          .utc()
          .add(7, "days")
      : moment()
          .utc()
          .add(1, "years");

    changeInputValue(
      "inductionExpiry",
      expiryDate.format("YYYY-MM-DDTHH:mm:ssZ")
    );
  };

  handleExpiryDateChanged = e => {
    if (e.value == null) return;
    this.handleChange(e);
    this.forceUpdate();
  };

  validatePassNumber = async params => {
    if (params.value === "") return;

    params.rule.isValid = true;
    params.rule.message = "This pass number has already been taken.";
    params.validator.validate();
  };

  render() {
    const {
      firstname,
      surname,
      contractor,
      trade,
      inductionDate,
      temporaryInduction,
      inductionExpiry,
      screeningRequired,
      screeningType,
      screeningDate,
      screeningExpiry,
      comments,
      operativePhotoUrl,
      editMode,
      passNumber
    } = this.props;

    const tradeDataSource = [
      {
        id: 1,
        value: "Builder"
      },
      { id: 2, value: "Painter" }
    ];

    const contractorDataSource = [
      {
        id: 1,
        value: "Test Co"
      },
      { id: 2, value: "Fake Inc" }
    ];

    const screeningTypeDataSource = [
      { id: 1, value: "Full" },
      { id: 2, value: "Temporary" }
    ];

    return (
      <React.Fragment>
        <form
          id="frmAddEditOperative"
          className={`container-fluid`}
          onSubmit={this.handleFormSubmit}
        >
          <ValidationGroup ref={this.validationGroup}>
            <div className="row pt-3">
              <div className="col-md-6">
                <div className="panel">
                  <h1 className="section-heading">Operative Details</h1>
                  <UniTextBox
                    name="firstname"
                    label="Firstname"
                    placeholder="Firstname"
                    onChange={this.handleChange}
                    value={firstname}
                  >
                    <RequiredRule message="Firstname is required." />
                    <StringLengthRule
                      min={2}
                      message="Firstname must be more than 2 characters."
                    />
                  </UniTextBox>
                  <UniTextBox
                    name="surname"
                    label="Surname"
                    placeholder="Surname"
                    onChange={this.handleChange}
                    value={surname}
                  >
                    <RequiredRule message="Surname is required." />
                    <StringLengthRule
                      min={2}
                      message="Surname must be more than 2 characters."
                    />
                  </UniTextBox>
                  <UniSelectBox
                    label="Company"
                    name="contractor"
                    dataSource={contractorDataSource}
                    onChange={this.handleChange}
                    value={contractor}
                  >
                    <RequiredRule message="Contractor is required." />
                  </UniSelectBox>
                  <div className="form-group" id="divTrade">
                    <label htmlFor="trade">Trade (Optional)</label>
                    <div className="input-group mb-3">
                      <SelectBox
                        id="trade"
                        dataSource={tradeDataSource}
                        valueExpr={"id"}
                        onValueChanged={e => this.handleChange(e)}
                        displayExpr={"value"}
                        className="full-width"
                        searchEnabled={true}
                        value={trade}
                      />
                      <div className="input-group-append">
                        <Button
                          width={100}
                          text={"Add New"}
                          type={"normal"}
                          stylingMode={"contained"}
                          onClick={() => this.handleShowPopup("addTradePopup")}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-6">
                <div className="panel">
                  <h1 className="section-heading">Induction</h1>
                  <div className="form-group" id="divInductionDate">
                    <label htmlFor="inductionDate">Induction Date</label>
                    <div className="input-group mb-3">
                      <DateBox
                        id={"inductionDate"}
                        defaultValue={this.now}
                        type={"date"}
                        onValueChanged={e => this.handleChange(e)}
                        className="full-width"
                        openOnFieldClick={true}
                        displayFormat="dd/MM/yyyy"
                        disabledDates={args => this.getDisabledDays(args)}
                        value={inductionDate}
                        editEnabled={false}
                        acceptCustomValue={false}
                        calendarOptions={{ firstDayOfWeek: 1 }}
                        dateSerializationFormat={"yyyy-MM-ddTHH:mm:ss"}
                        placeholder={"Induction Date"}
                      >
                        <Validator>
                          {this.renderRequiredValidation("Induction Date")}
                        </Validator>
                      </DateBox>
                    </div>
                  </div>
                  <div className="form-group" id="divTemporaryInduction">
                    <UniCheckBox
                      name="temporaryInduction"
                      label={"Temporary Induction"}
                      value={temporaryInduction}
                      onChange={this.handleTemporaryInductionChanged}
                    />
                  </div>
                  <UniDatePicker
                    label="Expiry Date"
                    name="inductionExpiry"
                    disabledDates={this.getDisabledDays}
                    onChange={this.handleExpiryDateChanged}
                    value={inductionExpiry}
                    placeholder={"Expiry Date"}
                  >
                    {this.renderRequiredValidation("Expiry Date")}
                  </UniDatePicker>
                  <div className="form-group" id="divComments">
                    <label htmlFor="comments">Comments (Optional)</label>
                    <div className="input-group mb-3">
                      <TextArea
                        id="comments"
                        className="full-width"
                        height={90}
                        maxLength={255}
                        onChange={e => this.handleChange(e)}
                        value={comments}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-6">
                <div className="panel">
                  <h1 className="section-heading">Screening</h1>
                  <div className="form-group" id="divScreeningRequired">
                    <UniCheckBox
                      name="screeningRequired"
                      label={"Screening Required"}
                      value={screeningRequired}
                      onChange={this.handleChange}
                    />
                  </div>
                  <UniSelectBox
                    name="screeningType"
                    label="Screening Type"
                    reference={this.screeningType}
                    dataSource={screeningTypeDataSource}
                    value={screeningType}
                    onChange={this.handleChange}
                    disabled={!screeningRequired}
                  >
                    {this.renderRequiredValidation("Screening Type")}
                  </UniSelectBox>
                  <UniDatePicker
                    label="Screening Date (Optional)"
                    name="screeningDate"
                    disabled={!screeningRequired}
                    disabledDates={this.getDisabledDays}
                    onChange={this.handleChange}
                    value={screeningDate}
                    placeholder={"Screening Date"}
                  />
                  <UniDatePicker
                    label="Screening Expiry (Optional)"
                    name="screeningExpiry"
                    disabled={!screeningRequired}
                    disabledDates={this.getDisabledDays}
                    onChange={this.handleChange}
                    value={screeningExpiry}
                    placeholder={"Screening Expiry"}
                  />
                </div>
              </div>
              <div className="col-md-6">
                <div className="panel">
                  <h1 className="section-heading">Card</h1>
                  {!editMode ? (
                    <UniTextBox
                      name="passNumber"
                      label="Pass Number (Optional)"
                      placeholder="Pass Number"
                      onChange={this.handleChange}
                      value={passNumber}
                      className={"pb-3"}
                    >
                      <NumericRule message={"Pass number must be a number."} />
                      <RangeRule
                        min={1}
                        max={2147483647}
                        message={"Pass number must be between 1 and 2147483647"}
                      />
                      <CustomRule
                        validationCallback={this.validatePassNumber}
                      />
                    </UniTextBox>
                  ) : (
                    <React.Fragment />
                  )}
                  {operativePhotoUrl === "" || operativePhotoUrl === null ? (
                    <React.Fragment>
                      <UniFileUploader
                        name={"ImageUpload"}
                        label={"Upload Image"}
                        multiple={false}
                        uploadUrl={"UploadOperativePhoto"}
                        onUpload={this.handleFileUploaded}
                      />
                    </React.Fragment>
                  ) : (
                    <div className={"row p-0 pl-3 pr-3"}>
                      <div className={"col-lg-6"}>
                        <div
                          className={"add-edit-operative-photo"}
                          style={{
                            backgroundImage: `url(${operativePhotoUrl})`
                          }}
                        />
                      </div>
                      <div className={"col-lg-6"}>
                        <UniFileUploader
                          name={"ImageUpload"}
                          label={"Upload Image"}
                          multiple={false}
                          uploadUrl={"UploadOperativePhoto"}
                          onUpload={this.updateOperativeFile}
                        />
                      </div>
                    </div>
                  )}
                </div>

                <div className={"add-edit-operative-submit-container"}>
                  <SubmitButtonsGroup
                    onClear={this.handleFormClear}
                    editMode={editMode || false}
                  />
                </div>
              </div>
            </div>
          </ValidationGroup>
        </form>
        <UniPopup
          title={"Add New Trade"}
          className={"popup-add-trade"}
          content={() => this.renderPopup(<AddTrade />)}
          popupName={"addTradePopup"}
        />
        <UniPopup
          title={"Print Card"}
          className={"popup-print-card"}
          content={() => this.renderPopup(<div />)}
          popupName={"printCardPopup"}
          onHiding={this.handleFormClear}
        />
      </React.Fragment>
    );
  }

  renderRequiredValidation = name => {
    return <RequiredRule message={`${name} is required.`} />;
  };

  renderPopup = content => {
    return <UniScrollView>{content}</UniScrollView>;
  };
}

const mapStateToProps = (state, ownProps) => {
  const { operativeDto, manageOperatives } = state;

  const {
    id,
    firstname,
    surname,
    contractor,
    trade,
    screeningType,
    screeningDate,
    screeningExpiry,
    inductionDate,
    temporaryInduction,
    inductionExpiry,
    comments,
    screeningRequired,
    filename,
    operativePhotoUrl,
    shouldComponentUpdate,
    editMode,
    passNumber,
    newPassNumberExists
  } = operativeDto;

  return {
    shouldComponentUpdate,
    selectedId: manageOperatives.selectedOperativeId,
    id,
    firstname,
    surname,
    contractor,
    trade,
    screeningType,
    screeningDate,
    screeningExpiry,
    inductionDate,
    temporaryInduction,
    inductionExpiry,
    comments,
    screeningRequired,
    filename,
    operativePhotoUrl,
    editMode: ownProps.editMode || editMode,
    passNumber,
    newPassNumberExists
  };
};

const mapDispatchToProps = dispatch => {
  return {
    togglePopup: popupName => {
      dispatch({ type: "TOGGLE_POPUP", popup: popupName });
    },
    changePageTitle: title => {
      dispatch({ type: "SET_PAGE_TITLE", title });
    },
    changeInputValue: (name, value) => {
      dispatch({ type: "CHANGE_INPUT_VALUE", name, value });
    },
    clearFormInputs: () => {
      dispatch({ type: "CLEAR_FORM_INPUTS" });
    },
    changeSelectedOperativeData: data => {
      dispatch({ type: "SET_SELECTED_OPERATIVE_DATA", data });
    },
    changeAllInputValues: operative => {
      dispatch({ type: "CHANGE_ALL_INPUT_VALUES", operative });
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddEditOperative);
