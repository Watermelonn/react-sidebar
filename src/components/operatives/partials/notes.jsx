import React, { Component } from "react";
import DataGrid, {
  Selection,
  Column,
  Scrolling
} from "devextreme-react/data-grid";
import { Button, TextArea } from "devextreme-react";
import Validator, {
  StringLengthRule,
  RequiredRule
} from "devextreme-react/validator";

class Notes extends Component {
  handleNotesSubmit = e => {
    e.preventDefault();
  };

  render() {
    const notes = [
      {
        date: "2019-07-03T12:09:14+0000",
        note: "Access removed due to expired competency.",
        creator: "Auto"
      },
      {
        date: "2019-07-02T12:00:00+0000",
        note: "Manuel Swipe Inserted.",
        creator: "D. Cooper"
      },
      {
        date: "2019-07-01T10:00:00+0000",
        note: "Operative booked on induction.",
        creator: "D. Cooper"
      },
      {
        date: "2019-07-01T10:00:00+0000",
        note: "Default site access granted.",
        creator: "Auto"
      },
      {
        date: "2019-07-01T09:00:00+0000",
        note: "Operative Created.",
        creator: "D. Cooper"
      },
      {
        date: "2019-07-03T12:09:14+0000",
        note: "Access removed due to expired competency.",
        creator: "Auto"
      },
      {
        date: "2019-07-02T12:00:00+0000",
        note: "Manuel Swipe Inserted.",
        creator: "D. Cooper"
      },
      {
        date: "2019-07-01T10:00:00+0000",
        note: "Operative booked on induction.",
        creator: "D. Cooper"
      },
      {
        date: "2019-07-01T10:00:00+0000",
        note: "Default site access granted.",
        creator: "Auto"
      },
      {
        date: "2019-07-01T09:00:00+0000",
        note: "Operative Created.",
        creator: "D. Cooper"
      },

      {
        date: "2019-07-03T12:09:14+0000",
        note: "Access removed due to expired competency.",
        creator: "Auto"
      },
      {
        date: "2019-07-02T12:00:00+0000",
        note: "Manuel Swipe Inserted.",
        creator: "D. Cooper"
      },
      {
        date: "2019-07-01T10:00:00+0000",
        note: "Operative booked on induction.",
        creator: "D. Cooper"
      },
      {
        date: "2019-07-01T10:00:00+0000",
        note: "Default site access granted.",
        creator: "Auto"
      },
      {
        date: "2019-07-01T09:00:00+0000",
        note: "Operative Created.",
        creator: "D. Cooper"
      }
    ];

    return (
      <React.Fragment>
        <div className="row pt-3 pl-3 pr-3">
          <div className="col-md-12 notes-table-container">
            <DataGrid
              className={"notes-table"}
              dataSource={notes}
              showBorders={true}
              showColumnLines={false}
              hoverStateEnabled={true}
              height={300}
            >
              <Scrolling mode={"virtual"} />
              <Selection mode={"none"} />
              <Column
                dataField={"date"}
                dataType={"date"}
                format={"dd/MM/yyyy HH:mm"}
                sortOrder={"desc"}
                width={150}
              />
              <Column dataField={"creator"} width={150} />
              <Column dataField={"note"} />
            </DataGrid>
          </div>
        </div>
        <div className="row pl-3 pr-3">
          <form className="col-md-12" onSubmit={e => this.handleNotesSubmit(e)}>
            <div className="form-group" id="divNotes">
              <hr/>
              <label htmlFor="txtNotes">Note</label>
              <div className="input-group mb-3">
                <TextArea
                  id="txtNotes"
                  className="full-width"
                  height={90}
                  maxLength={255}
                  placeholder={"Note"}
                >
                  <Validator>
                    <RequiredRule message="Note is required." />
                    <StringLengthRule
                      max={255}
                      message={"Note must less than 255 characters."}
                    />
                  </Validator>
                </TextArea>
              </div>
            </div>
            <div>
              <Button
                className={"notes-button-ok"}
                width={90}
                text={"Add"}
                type={"normal"}
                stylingMode={"contained"}
                useSubmitBehavior={true}
              />
            </div>
          </form>
        </div>
      </React.Fragment>
    );
  }
}

export default Notes;
