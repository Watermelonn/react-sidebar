import React, { Component } from "react";
import { connect } from "react-redux";
import { Button } from "devextreme-react";
import DataGrid, {
  Selection,
  Column,
  Scrolling
} from "devextreme-react/data-grid";
import { Validator, RequiredRule, RangeRule } from "devextreme-react/validator";
import DxSelectBox from "../../common/formElements/dxSelectBox";
import DxTextBox from "../../common/formElements/dxTextBox";
import DxDatePicker from "../../common/formElements/dxDatePicker";
import DxFileUploader from "../../common/formElements/dxFileUploader";

class Competencies extends Component {
  handleSubmit = e => {
    e.preventDefault();
  };

  handleSelectionChanged = () => {
  };

  render() {
    const addedCompetencies = [
      {
        name: "CSCS",
        regNo: "0000001",
        expiryDate: "2019-06-29T10:36:27+0000",
        testDate: "",
        fileUploaded: true
      },
      {
        name: "CSCS",
        regNo: "123456",
        expiryDate: "2019-09-01T10:36:27+0000",
        testDate: "",
        fileUploaded: true
      },
      {
        name: "Training Course",
        regNo: "0000002",
        expiryDate: "2019-06-28T10:36:27+0000",
        testDate: "2019-06-27T10:36:27+0000",
        fileUploaded: false
      },
      {
        name: "Training Course 2",
        regNo: "234567",
        expiryDate: "2019-06-28T10:36:27+0000",
        testDate: "",
        fileUploaded: false
      }
    ];

    const competencies = [
      {
        id: 1,
        value: "CSCS"
      },
      {
        id: 2,
        value: "CSCS Black"
      },
      {
        id: 3,
        value: "CSCS Blue"
      },
      {
        id: 4,
        value: "CSCS Gold"
      }
    ];

    return (
      <React.Fragment>
        <div className="row pt-0">
          <div className="col-md-12 competencies-container">
            <DataGrid
              dataSource={addedCompetencies}
              showBorders={true}
              showColumnLines={true}
              hoverStateEnabled={true}
              onSelectionChanged={() => this.handleSelectionChanged()}
            >
              <Scrolling mode={"virtual"} />
              <Selection mode={"single"} />
              <Column dataField={"name"} />
              <Column dataField={"regNo"} />
              <Column
                dataField={"expiryDate"}
                dataType={"date"}
                format={"dd/MM/yyyy"}
              />
              <Column
                dataField={"testDate"}
                dataType={"date"}
                format={"dd/MM/yyyy"}
              />
              <Column dataField={"fileUploaded"} />
            </DataGrid>
          </div>
        </div>
        <hr />
        <form onSubmit={e => this.handleSubmit(e)}>
          <div className="row pt-2">
            <div className="col-md-6">
              <DxSelectBox
                label={"Competency Name"}
                name={"CompetencyName"}
                items={competencies}
              >
                <Validator>
                  <RequiredRule message="Competency Name is required." />
                </Validator>
              </DxSelectBox>
              <DxDatePicker label="Expiry Date" name="ExpiryDate" className>
                <Validator>
                  <RequiredRule message="Expiry Date is required." />
                  <RangeRule
                    min={new Date().setDate(new Date().getDate() - 1)}
                    message="Expiry Date must not be earlier than today."
                  />
                </Validator>
              </DxDatePicker>
              <DxFileUploader
                name={"FileUpload"}
                label={"Upload File"}
                multiple={false}
              />
            </div>
            <div className="col-md-6">
              <DxTextBox
                label={"Registration No"}
                name={"RegistrationNumber"}
                placeholder={"Registration"}
              >
                <Validator>
                  <RequiredRule message="Registration Number is required." />
                </Validator>
              </DxTextBox>
              <DxDatePicker label="Test Date" name="TestDate" />
            </div>
          </div>
          {this.renderButtons()}
        </form>
      </React.Fragment>
    );
  }

  renderButtons() {
    if (this.props.editMode) {
      return (
        <React.Fragment>
          <Button
            text={"Delete"}
            type={"danger"}
            width={90}
            className="competencies-delete-button"
          />
          <Button
            width={90}
            text={"Update"}
            type={"normal"}
            stylingMode={"contained"}
            className="competencies-add-button"
            useSubmitBehavior={true}
          />
          <Button
            width={90}
            text={"Cancel"}
            type={"normal"}
            stylingMode={"contained"}
            className="competencies-clear-button"
          />
        </React.Fragment>
      );
    }
    return (
      <React.Fragment>
        <Button
          text={"Delete"}
          type={"danger"}
          disabled={true}
          width={90}
          className="competencies-delete-button"
        />
        <Button
          width={90}
          text={"Add"}
          type={"normal"}
          stylingMode={"contained"}
          className="competencies-add-button"
          useSubmitBehavior={true}
        />
        <Button
          width={90}
          text={"Clear"}
          type={"normal"}
          stylingMode={"contained"}
          className="competencies-clear-button"
        />
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  const { operatives } = state;
  return {
    editMode: operatives.addEditCompetencies.editMode
  };
};

export default connect(mapStateToProps)(Competencies);
