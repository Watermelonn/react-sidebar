import React, { Component } from "react";
import { connect } from "react-redux";
import UniTextBox from "../../common/forms/uniTextBox";
import { Button } from "devextreme-react";
class AddTrade extends Component {
  handleAddTrade = async () => {};

  shouldComponentUpdate(prevProps) {
    return (
      prevProps.tradeDescriptionExists !== this.props.tradeDescriptionExists
    );
  }

  handleTradeChanged = async e => {
    this.handleChange(e);
    this.props.changeInputValue("tradeDescriptionExists", false);
  };

  handleChange = e => {
    const name = e.element.id;
    const value = e.event.target.value;

    this.props.changeInputValue(name, value);
  };

  render() {
    return (
      <React.Fragment>
        <form value={"Trade"}>
          <div className={"row pl-3 pr-3 pt-1"}>
            <div className={"col-md-12"}>
              <UniTextBox
                label={"Trade Name"}
                name={"tradeDescription"}
                placeholder={"Trade Name"}
                onChange={this.handleTradeChanged}
              />
              <span className="add-trade-confirmation">
                {this.props.tradeDescriptionExists
                  ? "This trade already exists."
                  : ""}
              </span>
            </div>
          </div>
          <div className={"pr-3 pt-3"}>
            <Button
              width={90}
              text={"Add"}
              type={"default"}
              stylingMode={"contained"}
              className={"ml-2 pull-right"}
              value={"Trade"}
              onClick={this.handleAddTrade}
              disabled={this.props.tradeDescriptionExists}
            />
          </div>
        </form>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  const { operativeDto } = state;
  return {
    tradeDescription: operativeDto.tradeDescription,
    tradeDescriptionExists: operativeDto.tradeDescriptionExists
  };
};

const mapDispatchToProps = dispatch => {
  return {
    changeInputValue: (name, value) => {
      dispatch({ type: "CHANGE_INPUT_VALUE", name, value });
    },
    togglePopup: popupName => {
      dispatch({ type: "TOGGLE_POPUP", popup: popupName });
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddTrade);
