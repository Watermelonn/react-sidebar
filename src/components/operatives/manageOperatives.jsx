import React, { Component } from "react";
import { connect } from "react-redux";
import UniTable from "../common/tables/uniTable";
import UniPopup from "../common/popups/uniPopup";
import UniScrollView from "../common/forms/uniScrollView";
import TableButtonGroup from "../common/tables/tableButtonGroup";
import OkCancelPopup from "../common/popups/okCancelPopup";
import popups from "../../constants/manageOperativesPopups";
import { Switch } from "devextreme-react";
import { Column } from "devextreme-react/tree-list";
import moment from "moment";
import {testData1, testData2} from "../../testData/operatives";

class ManageOperatives extends Component {
  componentDidMount() {
    this.props.setPageTitle("Manage Operatives");
  }

  handleShowPopup = popupName => {
    this.props.togglePopup(popupName);
  };

  toggleShowDeleteDialog = () => {
    this.props.togglePopup("okCancelPopup");
  };

  handleDeleteOperative = async () => {
  
  };

  handleSelectionChanged = async ({ selectedRowsData }) => {
  
  };

  handleGetOperatives = () => {
    const { showInactive } = this.props;
    if (showInactive) return testData1;

    return testData2;
  };

  handleActiveOperativesSwitchChanged = () => {
    this.props.toggleShowInactiveOperatives();
    this.forceUpdate();
  };

  shouldComponentUpdate(nextProps) {
    const { refresh, toggleManageOperativesRefresh } = this.props;

    if (refresh) {
      toggleManageOperativesRefresh();
      return true;
    }
    return false;
  }

  toggleShowInductPopup = () => {
    const { togglePopup } = this.props;
    togglePopup("inductPopup");
    this.forceUpdate();
  };

  handleInductOperative = async () => {
  
  };

  render() {
    const editButtons = [
      {
        id: 1,
        width: 150,
        text: "View Operative",
        type: "normal",
        onClick: this.handleShowPopup,
        onClickParams: "editOperativePopup"
      },
      {
        id: 2,
        width: 150,
        text: "Notes",
        type: "normal",
        onClick: this.handleShowPopup,
        onClickParams: "notesPopup"
      },
      {
        id: 3,
        width: 150,
        text: this.props.showInactive ? "Restore" : "Delete",
        type: "normal",
        onClick: this.handleShowPopup,
        onClickParams: "okCancelPopup"
      },
      {
        id: 4,
        width: 150,
        text: "Print Card",
        type: "normal",
        onClick: this.handleShowPopup,
        onClickParams: "printCardPopup"
      },
      {
        id: 5,
        width: 150,
        text: "Induct",
        onClick: this.toggleShowInductPopup
      }
    ];

    const {
      showInactive,
      inductionExpiry,
      temporaryInduction
    } = this.props;

    return (
      <React.Fragment>
        <div className="container-fluid pt-3">
          <div className="row pt-0">
            <div className="col-md-12 manage-operatives-table-container">
              <Switch
                switchedOffText={"Deleted"}
                switchedOnText={"Active"}
                defaultValue={true}
                width={80}
                height={30}
                className={"manage-operatives-switch"}
                onValueChanged={() =>
                  this.handleActiveOperativesSwitchChanged()
                }
              />
              <UniTable
                data={this.handleGetOperatives()}
                alternatingRowColours={false}
                onDoubleClick={() => this.handleShowPopup("editOperativePopup")}
                onSelectionChanged={this.handleSelectionChanged}
                exportName={"Operatives"}
                exportEnabled={true}
              >
                <Column dataField={"id"} visible={false} />
                <Column dataField={"firstname"} />
                <Column dataField={"surname"} />
                <Column dataField={"company"} />
                <Column dataField={"reportingTo"} />
                <Column dataField={"trade"} />
                <Column dataField={"passNumber"} />
                <Column dataField={"inducted"} />
                <Column
                  dataField={"inductionDate"}
                  dataType={"date"}
                  format={"dd/MM/yyyy"}
                />
                <Column
                  dataField={"screeningDate"}
                  dataType={"date"}
                  format={"dd/MM/yyyy"}
                />
              </UniTable>
            </div>
          </div>
          {popups.map(item => (
            <UniPopup
              key={item.id}
              title={item.title}
              className={item.className}
              content={() => this.renderPopup(item.content)}
              popupName={item.popupName}
            />
          ))}
          {showInactive ? (
            <OkCancelPopup
              title={"Restore Operative"}
              popupName={"okCancelPopup"}
              message={`Are you sure want to restore this operative?`}
              dangerous={false}
              cancelText={"Cancel"}
              okText={"Restore"}
              onOkClick={this.handleDeleteOperative}
              onCancelClick={this.toggleShowDeleteDialog}
            />
          ) : (
            <OkCancelPopup
              title={"Delete Operative"}
              message={`Are you sure want to delete this operative?`}
              popupName={"okCancelPopup"}
              dangerous={true}
              cancelText={"Cancel"}
              okText={"Delete"}
              onOkClick={this.handleDeleteOperative}
              onCancelClick={this.toggleShowDeleteDialog}
            />
          )}
          <OkCancelPopup
            title={"Induct Operative"}
            message={
              temporaryInduction
                ? `TEMPORARY induction expiry is set for ${moment(
                    inductionExpiry
                  ).format(
                    "DD/MM/YYYY"
                  )}. Are you sure you wish to induct this operative?`
                : `Induction expiry is set for ${moment(inductionExpiry).format(
                    "DD/MM/YYYY"
                  )}. Are you sure you wish to induct this operative?`
            }
            popupName={"inductPopup"}
            dangerous={true}
            cancelText={"Cancel"}
            okText={"Induct"}
            onOkClick={this.handleInductOperative}
            onCancelClick={this.toggleShowInductPopup}
            className={"popup-induct"}
          />
          <div className="row buttons-row">
            <div className={"col-md-12"}>
              <div className={"row p-0 m-0"}>
                
                  <div className="col-md-12 p-0">
                    <TableButtonGroup
                      buttons={editButtons}
                      heading="Edit"
                      type={"manageOperatives"}
                      tableName={"manageOperativesTable"}
                    />
                  </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }

  renderPopup = content => {
    return <UniScrollView>{content}</UniScrollView>;
  };
}

const mapStateToProps = state => {
  const { manageOperatives, operativeDto } = state;

  const {
    refresh,
    manageOperativesTable,
    selectedOperativeId
  } = manageOperatives;

  const {
    firstname,
    surname,
    inductionExpiry,
    temporaryInduction
  } = operativeDto;

  return {
    selectedOperativeId,
    showInactive: manageOperativesTable.showInactive,
    buttonsDisabled: manageOperativesTable.buttonsDisabled,
    refresh,
    firstname,
    surname,
    inductionExpiry,
    temporaryInduction
  };
};

const mapDispatchToProps = dispatch => {
  return {
    changeSelectedOperative: id => {
      dispatch({ type: "CHANGE_SELECTED_OPERATIVE", id });
    },
    enableTableButtons: () => {
      dispatch({ type: "ENABLE_MANAGE_OPERATIVES_TABLE_BUTTONS" });
    },
    togglePopup: popupName => {
      dispatch({ type: "TOGGLE_POPUP", popup: popupName });
    },
    setPageTitle: title => {
      dispatch({ type: "SET_PAGE_TITLE", title });
    },
    changeAllInputValues: operative => {
      dispatch({ type: "CHANGE_ALL_INPUT_VALUES", operative });
    },
    toggleManageOperativesRefresh: () => {
      dispatch({ type: "TOGGLE_MANAGE_OPERATIVES_REFRESH" });
    },
    toggleShowInactiveOperatives: () => {
      dispatch({ type: "TOGGLE_SHOW_INACTIVE_OPERATIVES" });
    },
    clearFormInputs: () => {
      dispatch({ type: "CLEAR_FORM_INPUTS" });
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ManageOperatives);
