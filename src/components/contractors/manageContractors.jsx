import React, { Component } from "react";
import { connect } from "react-redux";
import { Switch } from "devextreme-react";
import { Column } from "devextreme-react/data-grid";
import UniTable from "../common/tables/uniTable";
import TableButtonGroup from "../common/tables/tableButtonGroup";
import OkCancelPopup from "../common/popups/okCancelPopup";
import { testData1, testData2 } from "../../testData/companies";

class ManageContractors extends Component {
  componentDidMount() {
    this.props.setPageTitle("Manage Companies");
  }

  handleActiveContractorsSwitchChanged = () => {
    this.props.toggleShowInactiveContractors();
    this.forceUpdate();
  };

  shouldComponentUpdate() {
    const { refresh, toggleManageContractorsRefresh } = this.props;

    if (refresh) {
      toggleManageContractorsRefresh();
      return true;
    }
    return false;
  }

  handleSelectionChanged = async ({ selectedRowsData }) => {};

  handleEditContractor = () => {
    this.props.history.push("/contractors/addNew?editMode=true");
  };

  handleDeleteContractor = async () => {};

  getContractorDataSource = () => {
    const showInactive = this.props.showInactive;

    if (showInactive) return testData1;

    return testData2;
  };

  handleDeleteContractorClicked = () => {
    this.props.togglePopup("okCancelPopup");
  };

  handleHoldContractorClicked = () => {
    this.props.togglePopup("holdContractorPopup");
  };

  handleToggleHold = async () => {};

  checkForOnHold = e => {
    if (!e.data) return;
    if (e.data.currentStatus === "On Hold") {
      e.cellElement.className += " red";
    }
  };

  render() {
    const { onHold, showInactive } = this.props;

    const editButtons = [
      {
        id: 1,
        width: 150,
        text: "Edit Contractor",
        type: "normal",
        onClick: this.handleEditContractor
      },
      {
        id: 3,
        width: 150,
        text: "Delete",
        type: "normal",
        onClick: this.handleDeleteContractorClicked,
        onClickParams: "deleteContractorPopup",
        hidden: showInactive
      },
      {
        id: 4,
        width: 150,
        text: "Hold",
        type: "normal",
        onClick: this.handleHoldContractorClicked,
        onClickParams: "holdContractorPopup",
        hidden: showInactive
      }
    ];

    return (
      <React.Fragment>
        <div className="container-fluid pt-3">
          <div className="row pt-0">
            <div className="col-md-12 manage-contractor-table-container">
              <Switch
                switchedOffText={"Inactive"}
                switchedOnText={"Active"}
                defaultValue={true}
                width={80}
                height={30}
                className={"manage-contractors-switch"}
                onValueChanged={() =>
                  this.handleActiveContractorsSwitchChanged()
                }
                value={!showInactive}
              />
              <UniTable
                data={this.getContractorDataSource()}
                alternatingRowColours={false}
                onDoubleClick={this.handleEditContractor}
                onSelectionChanged={this.handleSelectionChanged}
                exportName={"Contractors"}
                exportEnabled={true}
                onCellPrepared={this.checkForOnHold}
              >
                <Column dataField={"name"} />
                <Column dataField={"contactName"} />
                <Column dataField={"telephone"} />
                <Column
                  dataField={"renewalDate"}
                  dataType={"date"}
                  format={"dd/MM/yyyy"}
                />
                <Column
                  dataField={"insuranceExpires"}
                  dataType={"date"}
                  format={"dd/MM/yyyy"}
                />
                <Column
                  dataField={"safeContractorExpiry"}
                  caption={"Safe Contractor"}
                  dataType={"date"}
                  format={"dd/MM/yyyy"}
                />
                <Column dataField={"type"} />
                <Column dataField={"reportingTo"} />
                <Column dataField={"currentStatus"} />
              </UniTable>
            </div>
          </div>
          <div className="row buttons-row">
            <div className={"col-md-12"}>
              <div className={"row p-0 m-0"}>
                <div className="col-md-12 p-0">
                  <TableButtonGroup
                    buttons={editButtons}
                    heading="Edit"
                    type={"manageContractors"}
                    tableName={"manageContractorsTable"}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
        <OkCancelPopup
          className={"dialog-ok-cancel-contractor"}
          popupName={"okCancelPopup"}
          title={"Delete Contractor"}
          message={`Are you sure want to delete this contractor? This will also remove any associated operatives and is not reversible!`}
          dangerous={true}
          cancelText={"Cancel"}
          okText={"Delete"}
          onOkClick={this.handleDeleteContractor}
          onCancelClick={this.handleDeleteContractorClicked}
        />
        {onHold ? (
          <OkCancelPopup
            className={"dialog-ok-cancel-onhold"}
            popupName={"holdContractorPopup"}
            title={"Take Contractor Off Hold"}
            message={`Are you sure you wish to take this contractor off hold? Access will be granted for all operatives previously put on hold.`}
            dangerous={true}
            cancelText={"Cancel"}
            okText={"Take Off Hold"}
            okButtonWidth={150}
            onOkClick={this.handleToggleHold}
            onCancelClick={this.handleHoldContractorClicked}
          />
        ) : (
          <OkCancelPopup
            className={"dialog-ok-cancel-onhold"}
            popupName={"holdContractorPopup"}
            title={"Put Contractor On Hold"}
            message={`Are you sure you wish to put this contractor on hold? Access will be removed for all operatives employed by this company until they are put off hold.`}
            dangerous={true}
            cancelText={"Cancel"}
            okText={"Put On Hold"}
            okButtonWidth={150}
            onOkClick={this.handleToggleHold}
            onCancelClick={this.handleHoldContractorClicked}
          />
        )}
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  const { manageContractors } = state;

  const {
    refresh,
    manageContractorsTable,
    selectedContractorId
  } = manageContractors;

  return {
    selectedContractorId,
    showInactive: manageContractorsTable.showInactive,
    buttonsDisabled: manageContractorsTable.buttonsDisabled,
    refresh
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setPageTitle: title => {
      dispatch({ type: "SET_PAGE_TITLE", title });
    },
    changeSelectedContractor: id => {
      dispatch({ type: "CHANGE_SELECTED_CONTRACTOR", id });
    },
    enableTableButtons: () => {
      dispatch({ type: "ENABLE_MANAGE_CONTRACTORS_TABLE_BUTTONS" });
    },
    togglePopup: popupName => {
      dispatch({ type: "TOGGLE_POPUP", popup: popupName });
    },
    toggleManageContractorsRefresh: () => {
      dispatch({ type: "TOGGLE_MANAGE_CONTRACTORS_REFRESH" });
    },
    toggleShowInactiveContractors: () => {
      dispatch({ type: "TOGGLE_SHOW_INACTIVE_CONTRACTORS" });
    },
    clearFormInputs: () => {
      dispatch({ type: "CLEAR_CONTRACTOR_FORM_INPUTS" });
    },
    changeAllInputValues: contractor => {
      dispatch({ type: "CHANGE_ALL_CONTRACTOR_INPUT_VALUES", contractor });
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ManageContractors);
