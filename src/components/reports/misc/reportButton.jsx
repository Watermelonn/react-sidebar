import React, { Component } from "react";
import { connect } from "react-redux";
import { Button } from "devextreme-react";
import { Tooltip } from "devextreme-react/tooltip";
import { withRouter } from "react-router-dom";

class ReportButton extends Component {
  constructor(props) {
    super(props);

    this.state = {
      popupVisible: false
    };
  }

  toggleShowPopup = () => {
    this.setState({
      popupVisible: !this.state.popupVisible
    });
  };

  reportButtonClick = () => {
    const { endpoint, history } = this.props;

    history.push(`/report?reportType=${endpoint}`);
  };

  toggleFavouriteReport = async () => {
    const { id, addOrRemoveFavouriteReport } = this.props;
    addOrRemoveFavouriteReport(id);
  };

  render() {
    const { id, text, tooltip, isFavourite } = this.props;

    return (
      <div
        onMouseEnter={() => this.toggleShowPopup()}
        onMouseLeave={() => this.toggleShowPopup()}
        className={"one-hundred-width pt-2 pb-2"}
      >
        <span
          className={`report-button-save-icon no-select ${
            isFavourite ? "filled" : ""
          }`}
          onClick={() => this.toggleFavouriteReport(id)}
        >
          ★
        </span>
        <Button
          id={`report${id}`}
          text={text}
          width={"100%"}
          height={120}
          type={"normal"}
          stylingMode={"outlined"}
          onClick={() => this.reportButtonClick()}
          render={buttonData => (
            <span className={"dx-button-text report-button-text"}>
              {buttonData.text}
            </span>
          )}
        />
        <Tooltip
          target={`#report${id}`}
          visible={this.state.popupVisible}
          closeOnOutsideClick={false}
          position={"right"}
        >
          <span>{tooltip}</span>
        </Tooltip>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  const { favouriteReports } = state;
  const { id } = ownProps;
  return {
    isFavourite: favouriteReports.reports.some(x => x === id)
  };
};

const mapDispatchToProps = dispatch => {
  return {
    addOrRemoveFavouriteReport: id => {
      dispatch({ type: "ADD_OR_REMOVE_FAVOURITE_REPORT", id });
    }
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ReportButton)
);
