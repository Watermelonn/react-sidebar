import React, { Component } from "react";
import { connect } from "react-redux";
import ReportButton from "./misc/reportButton";
import operativeReports from "../../constants/operativeReports";
import contractorReports from "../../constants/contractorReports";

class ReportsMenu extends Component {
  async componentDidMount() {
    const { setPageTitle } = this.props;
    setPageTitle("Reports");
  }

  render() {
    return (
      <div className={"container-fluid"}>
        <div className={"row pt-3"}>
          <div className={"col-lg-6"}>
            <div className={"panel operative-reports-container"}>
              <span>Operative Reports</span>
              <div className={"row p-0 pt-2"}>
                {operativeReports.map(
                  ({ id, endpoint, filters, text, tooltip }) => (
                    <div className={"col-lg-6"} key={id}>
                      <ReportButton
                        id={id}
                        endpoint={endpoint + "&filter=" + filters}
                        text={text}
                        tooltip={tooltip}
                      />
                    </div>
                  )
                )}
              </div>
            </div>
          </div>
          <div className={"col-lg-6"}>
            <div className={"panel contractor-reports-container"}>
              <span>Contractor Reports</span>
              <div className={"row p-0 pt-2"}>
                {contractorReports.map(
                  ({ id, endpoint, filters, text, tooltip }) => (
                    <div className={"col-lg-6"} key={id}>
                      <ReportButton
                        id={id}
                        endpoint={endpoint + "&filter=" + filters}
                        text={text}
                        tooltip={tooltip}
                      />
                    </div>
                  )
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    setPageTitle: title => {
      dispatch({ type: "SET_PAGE_TITLE", title });
    },
    setFavouriteReports: reports => {
      dispatch({ type: "SET_FAVOURITE_REPORTS", reports });
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ReportsMenu);
