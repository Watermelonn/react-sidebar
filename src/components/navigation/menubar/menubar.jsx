import React, { Component } from "react";
import { connect } from "react-redux";
import { ScrollView } from "devextreme-react";
import MenubarList from "./menubarList";
import Settings from "../../home/settings";
import UniPopup from "../../common/popups/uniPopup";
import OkCancelPopup from "../../common/popups/okCancelPopup";
import { faBars, faCog, faSignOutAlt } from "@fortawesome/free-solid-svg-icons";

class Menubar extends Component {
  shouldComponentUpdate(nextProps) {
    const { sidebarCollapsed, toggleExpandOptionList } = this.props;
    if (sidebarCollapsed !== nextProps.sidebarCollapsed) {
      toggleExpandOptionList(null);
    }
    return true;
  }

  handleMenuToggle = () => {
    const { cookies, sidebarCollapsed, toggleSidebarCollapsing } = this.props;

    const expiryDate = new Date();
    expiryDate.setFullYear(expiryDate.getFullYear() + 1);

    let collapsed = false;

    if (typeof sidebarCollapsed === "string") {
      collapsed = sidebarCollapsed === "true";
    } else {
      collapsed = sidebarCollapsed;
    }

    cookies.set("sidebarCollapsed", !collapsed, {
      path: "/",
      expires: expiryDate
    });

    toggleSidebarCollapsing();
    this.forceUpdate();
  };

  handleSettingsClicked = () => {
    this.props.togglePopup("userSettingsPopup");
  };

  handleToggleShowLogOutDialog = () => {
    this.props.togglePopup("logOutPopup");
  };

  handleLogOut = async () => {
    window.location.replace("https://gitlab.com/Watermelonn");
  };

  render() {
    const leftBarItems = [
      { id: 1, icon: faBars, onClick: this.handleMenuToggle }
    ];
    const rightBarItems = [
      { id: 1, icon: faCog, onClick: this.handleSettingsClicked },
      { id: 2, icon: faSignOutAlt, onClick: this.handleToggleShowLogOutDialog }
    ];

    const { title } = this.props;

    return (
      <div className="menubar">
        <div className="menubar-container">
          <div className="menubar-left-list-container">
            <MenubarList items={leftBarItems} />
          </div>
          <div className="menubar-mid-list-container">
            <span className="page-heading no-select">{title}</span>
          </div>
          <div className="menubar-right-list-container">
            <MenubarList items={rightBarItems} />
          </div>
        </div>
        <UniPopup
          title={"Settings"}
          className={"popup-settings"}
          content={this.renderSettingsPopup}
          popupName={"userSettingsPopup"}
        />
        <OkCancelPopup
          popupName={"logOutPopup"}
          title={"Log out"}
          message={"Are you sure you want to log out?"}
          dangerous={true}
          cancelText={"Cancel"}
          okText={"Ok"}
          onOkClick={this.handleLogOut}
          onCancelClick={this.handleToggleShowLogOutDialog}
        />
      </div>
    );
  }

  renderSettingsPopup = () => {
    return (
      <div className="scrollViewContainer">
        <ScrollView
          showScrollbar={"onScroll"}
          scrollByContent={true}
          scrollByThumb={true}
        >
          <Settings cookies={this.props.cookies} />
        </ScrollView>
      </div>
    );
  };
}

const mapStateToProps = (state, ownProps) => {
  const { navigation } = state;
  return {
    cookies: ownProps.cookies,
    sidebarCollapsed: ownProps.cookies.cookies.sidebarCollapsed,
    title: navigation.pageTitle
  };
};

const mapDispatchToProps = dispatch => {
  return {
    toggleSidebarCollapsing: () => {
      dispatch({ type: "TOGGLE_SIDEBAR_COLLAPSING" });
    },
    toggleExpandOptionList: id => {
      dispatch({ type: "TOGGLE_EXPAND_OPTION_LIST", id });
    },
    togglePopup: popup => {
      dispatch({ type: "TOGGLE_POPUP", popup });
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Menubar);
