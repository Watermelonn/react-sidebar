import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

class MenubarList extends Component {
  render() {
    const { items } = this.props;

    return (
      <ul className="menubar-list">
        {items.map(({ id, icon, onClick }) => (
          <li key={id} className="menubar-list-option" onClick={onClick}>
            <FontAwesomeIcon icon={icon} className="menubar-list-button" />
          </li>
        ))}
      </ul>
    );
  }
}

export default MenubarList;
