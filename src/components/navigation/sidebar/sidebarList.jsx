import React, { Component } from "react";
import SidebarOption from "./sidebarOption";
import SidebarOptionWithSubList from "./sidebarOptionWithSubList";

class SidebarList extends Component {
  render() {
    const { items, collapsed } = this.props;

    return (
      <ul className="sidebar-list">
        {items.map(item =>
          item.subList === undefined ? (
            <SidebarOption key={item.id} item={item} />
          ) : (
            <SidebarOptionWithSubList
              collapsed={collapsed}
              key={item.id}
              item={item}
            />
          )
        )}
      </ul>
    );
  }
}

export default SidebarList;
