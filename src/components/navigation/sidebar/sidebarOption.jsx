import React from "react";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { connect } from "react-redux";

const SidebarOption = ({ item }) => {
  const { id, name, icon, style, href } = item;

  return (
    <React.Fragment>
        <Link to={href}>
          <li key={id} className="sidebar-option no-select">
            <span className={"sidebar-option-link"}>
              <FontAwesomeIcon
                style={style}
                icon={icon}
                className={"sidebar-option-icon"}
              />
              <span className="sidebar-option-text">{name}</span>
            </span>
          </li>
        </Link>
    </React.Fragment>
  );
};

const mapStateToProps = state => {

};

export default connect(mapStateToProps)(SidebarOption);
