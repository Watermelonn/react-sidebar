import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHome } from "@fortawesome/free-solid-svg-icons";
import SidebarList from "./sidebarList";
import reactLogoWide from "../../../images/react_logo_wide.png";

class Sidebar extends Component {
  componentDidMount() {
    if (this.getSidebarCollapsed()) this.props.toggleExpandOptionList(0);
  }

  getSidebarCollapsed = () => {
    const { sidebarCollapsed } = this.props;
    let collapsed = false;

    if (typeof sidebarCollapsed === "string") {
      collapsed = sidebarCollapsed === "true";
    } else {
      collapsed = sidebarCollapsed;
    }

    return collapsed;
  };
  render() {
    const { mainMenu } = this.props;

    const collapsed = this.getSidebarCollapsed();
    const sidebarClassName = collapsed ? "collapsed" : "";

    const logo = reactLogoWide;
    return (
      <div className={"page-sidebar " + sidebarClassName}>
        <div className="sidebar-container">
          <Link to="/">
            <img
              alt="Home"
              className="react-logo-wide no-select"
              src={logo}
            />
            <div className="homeIconContainer">
              <FontAwesomeIcon icon={faHome} className="homeIcon" />
            </div>
          </Link>
          <div className="sidebar-list-container">
            <SidebarList collapsed={collapsed} items={mainMenu} />
          </div>
          <hr />
        </div>
  
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  const { navigation, theme } = state;
  const { sidebarItems, sidebarCollapsing } = navigation;
  return {
    mainMenu: sidebarItems.mainItems,
    secondaryMenu: sidebarItems.secondaryItems,
    sidebarCollapsed: ownProps.cookies.cookies.sidebarCollapsed,
    sidebarCollapsing: sidebarCollapsing,
    darkTheme: theme.darkTheme
  };
};

const mapDispatchToProps = dispatch => {
  return {
    toggleExpandOptionList: id => {
      dispatch({ type: "TOGGLE_EXPAND_OPTION_LIST", id });
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Sidebar);
