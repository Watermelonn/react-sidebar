import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

class SidebarSubList extends Component {
  componentDidUpdate(e) {
    const { toggleHoverStatus } = this.props;
    toggleHoverStatus(e.parentId || e);
  }

  handleSubListHover = (parentId, value) => {
    this.props.hoverSidebarOption(parentId, "subHover", value);
    this.componentDidUpdate(parentId);
  };

  render() {
    const { items, closed, allHover, parentId } = this.props;

    const sidebarSubListClassName =
      (closed ? "hidden " : "") + (allHover ? "pop-out" : "");

    return (
      <ul
        onMouseEnter={() => this.handleSubListHover(parentId, true)}
        onMouseLeave={() => this.handleSubListHover(parentId, false)}
        className={"sidebar-sublist " + sidebarSubListClassName}
      >
        {items.map(({ id, name, href }) => (
          <React.Fragment key={id}>
            <Link key={id} to={href}>
              <li className="sidebar-sublist-option no-select">
                <span className={"sidebar-sublist-option-link"}>{name}</span>
              </li>
            </Link>
          </React.Fragment>
        ))}
      </ul>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  const item = state.navigation.sidebarItems.mainItems.find(
    x => x.id === ownProps.parentId
  );
  return {
    allHover: item.allHover,
    subHover: item.subHover
  };
};

const mapDispatchToProps = dispatch => {
  return {
    hoverSidebarOption: (id, item, value) => {
      dispatch({ type: "TOGGLE_SIDEBAR_OPTION_HOVER", id, item, value });
    },
    toggleHoverStatus: id => {
      dispatch({ type: "TOGGLE_SIDEBAR_OPTION_SUBLIST_HOVER", id });
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SidebarSubList);
