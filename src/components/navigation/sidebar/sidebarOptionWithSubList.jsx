import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { connect } from "react-redux";
import SidebarSubList from "./sidebarSubList";
import Chevron from "./sidebarListChevron";

class SidebarOptionWithSubList extends Component {
  componentDidUpdate(e) {
    const { toggleHoverStatus } = this.props;
    setTimeout(function() {
      toggleHoverStatus(e.item ? e.item.id : e);
    }, 100);
  }

  handleSidebarHover = (id, value) => {
    this.props.hoverSidebarOption(id, "mainHover", value);
    this.componentDidUpdate(id);
  };

  handleClickOption = e => {
    const { collapsed, clickSidebarOption } = this.props;
    if (collapsed) {
      return;
    }
    clickSidebarOption(e.id);
  };

  render() {
    const { item, allHover, closed } = this.props;
    const { id, name, icon, style, subList } = item;

    const listItemClassName =
      (closed ? "" : " option-extended ") + (allHover ? "pop-out" : "");

    return (
      <React.Fragment>
        <React.Fragment>
          <li
            key={id}
            onMouseEnter={() => this.handleSidebarHover(id, true)}
            onMouseLeave={() => this.handleSidebarHover(id, false)}
            className={
              "sidebar-option sidebar-option-sublist " + listItemClassName
            }
          >
            <div
              className="sidebar-option-clickable-area"
              onClick={() => this.handleClickOption(item)}
            >
              <span className={"sidebar-option-link"}>
                <FontAwesomeIcon
                  style={style}
                  icon={icon}
                  className={"sidebar-option-icon"}
                />
                <span className="sidebar-option-sublist-text no-select">
                  {name}
                </span>
              </span>
              <Chevron closed={closed} />
            </div>
          </li>
          <SidebarSubList items={subList} parentId={id} closed={closed} />
        </React.Fragment>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  const item = state.navigation.sidebarItems.mainItems.find(
    x => x.id === ownProps.item.id
  );
  return {
    allHover: item.allHover,
    mainHover: item.mainHover,
    closed: item.closed
  };
};

const mapDispatchToProps = dispatch => {
  return {
    clickSidebarOption: id => {
      dispatch({ type: "TOGGLE_EXPAND_OPTION_LIST", id });
    },
    hoverSidebarOption: (id, item, value) => {
      dispatch({ type: "TOGGLE_SIDEBAR_OPTION_HOVER", id, item, value });
    },
    toggleHoverStatus: id => {
      dispatch({ type: "TOGGLE_SIDEBAR_OPTION_SUBLIST_HOVER", id });
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SidebarOptionWithSubList);
