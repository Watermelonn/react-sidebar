import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faChevronLeft,
  faChevronDown
} from "@fortawesome/free-solid-svg-icons";

class Chevron extends Component {
  render() {
    const { closed } = this.props;
    let icon = faChevronLeft;

    if (closed === false) {
      icon = faChevronDown;
    } else if (closed !== true && closed !== false) {
      return null;
    }

    return (
      <FontAwesomeIcon
        icon={icon}
        className="sidebar-main-option-icon sidebar-list-toggle"
      />
    );
  }
}

export default Chevron;
