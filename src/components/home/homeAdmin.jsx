import React, { Component } from "react";
import { connect } from "react-redux";
import moment from "moment";
import { List } from "devextreme-react";
import PieChart, {
  Label,
  Connector,
  SmallValuesGrouping
} from "devextreme-react/pie-chart";
import Chart, {
  Tooltip,
  Series,
  Legend,
  Size,
  ArgumentAxis
} from "devextreme-react/chart";
class HomeAdmin extends Component {
  componentDidMount() {
    this.props.changePageTitle("React Example");
  }

  render() {
    const numberOnSiteDataSource = [{
      date: "2019-11-25T00:00:00",
      number: 40
    },
    {
      date: "2019-11-26T00:00:00",
      number: 65
    },
    {
      date: "2019-11-27T00:00:00",
      number: 78
    },
    {
      date: "2019-11-28T00:00:00",
      number: 8
    },
    {
      date: "2019-11-29T00:00:00",
      number: 6
    },
    {
      date: "2019-11-30T00:00:00",
      number: 102
    }];

    const numberOnSiteByContractorDataSource = [{
      contractorName: "Fake Inc",
      number: 56
    },
    {
      contractorName: "Test Co",
      number: 29
    },
    {
      contractorName: "Acme",
      number: 67
    },
    {
      contractorName: "Not Real Ltd",
      number: 89
    },
    {
      contractorName: "Dan Co",
      number: 12
    },
    {
      contractorName: "Did you really think this was real?",
      number: 30
    }];

    const temporaryInductionDataSource = 
      [{
        trade: "Builder",
        number: 12
      },
      {
        trade: "Painter",
        number: 100
      },
      {
        trade: "Paint Stripper",
        number: 90
      },
      {
        trade: "Tester",
        number: 48
      },
      {
        trade: "Plumber",
        number: 23
      },
      {
        trade: "Ladder Mover",
        number: 65
      },
      {
        trade: "Interviewer",
        number: 86
      }];

    const changelogDataSource = [{
      date: "2019-11-25T00:00:00",
      note: "Fixed a minor bug."
    },
    {
      date: "2019-11-26T00:00:00",
      note: "Oops, looked like that bug came back."
    },
    {
      date: "2019-11-27T00:00:00",
      note: "Ok, it's definitely fixed now!"
    },
    {
      date: "2019-11-28T00:00:00",
      note: "Not."
    },
    {
      date: "2019-11-29T00:00:00",
      note: "Ok, try now."
    }];

    return (
      <div className={"container-fluid"}>
        <div className={"row pt-3"}>
          <div className={"col-lg-6 homepage-panel"}>
            <div className={"panel"}>
              <h1 className={"section-heading"}>Total Number of Operatives on Site</h1>
              <Chart dataSource={numberOnSiteDataSource}>
                <Series
                  type={"line"}
                  argumentField={"date"}
                  valueField={"number"}
                />
                <ArgumentAxis
                  argumentType={"datetime"}
                  label={{ format: "dd/MM/yyyy" }}
                />
                <Legend visible={false} />
                <Tooltip enabled={true} />
                <Size height={320} />
              </Chart>
            </div>
          </div>
          <div className={"col-lg-6 homepage-panel"}>
            <div className={"panel"}>
              <h1 className={"section-heading"}>
                Number on Site by Company
              </h1>
              <PieChart dataSource={numberOnSiteByContractorDataSource}>
                <Series argumentField={"contractorName"} valueField={"number"}>
                  <Label visible={true}>
                    <Connector visible={true} width={2} />
                  </Label>
                </Series>
                <Tooltip
                  enabled={true}
                  customizeTooltip={HomeAdmin.customiseTooltip}
                />
                <SmallValuesGrouping />
                <Size height={320} />
              </PieChart>
            </div>
          </div>
        </div>
        <div className={"row"}>
          <div className={"col-lg-6 homepage-panel"}>
            <div className={"panel"}>
              <h1 className={"section-heading"}>
                Inductions this Week by Trade
              </h1>
              <PieChart
                dataSource={temporaryInductionDataSource}
                type={"doughnut"}
              >
                <Series argumentField={"trade"} valueField={"number"}>
                  <Label visible={true}>
                    <Connector visible={true} width={2} />
                  </Label>
                </Series>
                <SmallValuesGrouping />
                <Size height={320} />
                <Tooltip
                  enabled={true}
                  customizeTooltip={HomeAdmin.customiseTooltip}
                />
              </PieChart>
            </div>
          </div>
          <div className={"col-lg-6 homepage-panel"}>
            <div className={"panel"}>
              <h1 className={"section-heading"}>Recent Updates</h1>
              <List
                dataSource={changelogDataSource}
                height={320}
                showSelectionControls={false}
                selectionMode={"none"}
                itemRender={this.renderUpdateItem}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }

  static customiseTooltip(arg) {
    return {
      text: arg.argumentText
    };
  }

  renderUpdateItem = item => {
    return (
      <React.Fragment>
        <span className={"pr-3"}>
          <b>{moment(item.date).format("DD/MM/YYYY")}</b>
        </span>
        <span>{item.note}</span>
        <span className={"pull-right badge badge-secondary"}>
          {item.versionNumber}
        </span>
      </React.Fragment>
    );
  };
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    changePageTitle: title => {
      dispatch({ type: "SET_PAGE_TITLE", title });
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeAdmin);
