import React, { Component } from "react";
import { connect } from "react-redux";
import UniSwitch from "../common/forms/uniSwitch";
import { Button } from "devextreme-react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrash } from "@fortawesome/free-solid-svg-icons";
import notify from "devextreme/ui/notify";

class Settings extends Component {
  handleThemeChanged = e => {
    const { cookies, toggleTheme } = this.props;

    const expiryDate = new Date();
    expiryDate.setFullYear(expiryDate.getFullYear() + 1);

    cookies.set("darkTheme", e.value, {
      path: "/",
      expires: expiryDate
    });

    toggleTheme();
  };

  resetFavouriteReports = async () => {
    const { setFavouriteReports } = this.props;
    notify("Favourite Reports Reset", "success", 1500);
    setFavouriteReports([]);
  };

  render() {
    const { darkTheme } = this.props;
    return (
      <React.Fragment>
        <div className="row pt-0">
          <div className="col-md-12 settings-container">
            <UniSwitch
              name="ThemeSwitch"
              label="Dark Theme"
              value={darkTheme}
              onValueChanged={this.handleThemeChanged}
            />
        
              <React.Fragment>
                <hr />
                <div className="input-group">
                  <label className={"pr-4"}>Reset Favourite Reports</label>
                  <Button
                    width={90}
                    height={32}
                    text={"Reset"}
                    type={"danger"}
                    stylingMode={"contained"}
                    className={"reset-reports-button"}
                    render={buttonData => (
                      <React.Fragment>
                        <span className={"dx-button-text"}>
                          <FontAwesomeIcon icon={faTrash} className={"mr-1"} />
                          {buttonData.text}
                        </span>
                      </React.Fragment>
                    )}
                    onClick={this.resetFavouriteReports}
                  />
                </div>
              </React.Fragment>
         
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  const { theme } = state;
  return {
    darkTheme: theme.darkTheme
  };
};

const mapDispatchToProps = dispatch => {
  return {
    toggleTheme: () => {
      dispatch({ type: "TOGGLE_THEME" });
    },
    setFavouriteReports: reports => {
      dispatch({ type: "SET_FAVOURITE_REPORTS", reports });
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Settings);
