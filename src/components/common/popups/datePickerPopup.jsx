import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Validator, { RequiredRule } from "devextreme-react/validator";
import { Button, DateBox } from "devextreme-react";
import UniPopup from "./uniPopup";

class DatePickerPopup extends Component {
  constructor(props) {
    super(props);
    this.renderDialog = this.renderDialog.bind(this);
  }

  handleCloseDialog = () => {
    this.props.togglePopup("datePickerPopup");
  };

  handleClick = () => {
    const { date, onOkClick } = this.props;

    onOkClick(date);
    this.handleCloseDialog();
  };

  handleDateChange = e => {
    const value = e.value;

    this.props.changeDatePickerDate(value);
  };

  render() {
    const { title, popupName } = this.props;
    return (
      <UniPopup
        title={title}
        className={"dialog-date-picker"}
        popupName={popupName}
        content={this.renderDialog}
      />
    );
  }

  renderDialog() {
    const { label, okText, disabledDates } = this.props;

    return (
      <React.Fragment>
        <div className={"row  pt-1 pl-3 pr-3"}>
          <div className={"col-md-12"}>
            <div className={"form-group"}>
              <label htmlFor={"swipeDateTime"}>{label}</label>
              <DateBox
                type={"datetime"}
                width={"100%"}
                openOnFieldClick={true}
                displayFormat={"dd/MM/yyyy HH:mm"}
                id={"swipeDateTime"}
                placeholder={"Date"}
                dateSerializationFormat={"yyyy-MM-ddTHH:mm:ss"}
                disabledDates={this.getDisabledDates}
                onValueChanged={e => this.handleDateChange(e)}
                disabledDates={disabledDates}
              >
                <Validator>
                  <RequiredRule message={`${label} is required.`} />
                </Validator>
              </DateBox>
            </div>
          </div>
        </div>
        <Button
          className={"dialog-button-ok pull-right mr-3"}
          type={"normal"}
          width={90}
          text={okText}
          onClick={() => this.handleClick()}
        />
      </React.Fragment>
    );
  }
}

DatePickerPopup.propTypes = {
  popupName: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  label: PropTypes.string,
  cancelText: PropTypes.string,
  okText: PropTypes.string,
  onOkClick: PropTypes.func.isRequired,
  restrictFutureDates: PropTypes.bool,
  disabledDates: PropTypes.func
};

const mapStateToProps = state => {
  const { popups } = state;
  return {
    date: popups.datePickerPopup.date
  };
};

const mapDispatchToProps = dispatch => {
  return {
    togglePopup: popup => {
      dispatch({ type: "TOGGLE_POPUP", popup });
    },
    changeDatePickerDate: date => {
      dispatch({ type: "CHANGE_DATE_PICKER_DATE", date });
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DatePickerPopup);
