import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Popup } from "devextreme-react";

class UniPopup extends Component {
  handleTogglePopup = popupName => {
    const { onHiding, togglePopup } = this.props;
    if (onHiding) onHiding();
    togglePopup(popupName);
  };

  render() {
    const { visible, title, content, className, popupName } = this.props;

    return (
      <Popup
        visible={visible}
        showTitle={true}
        title={title}
        dragEnabled={false}
        closeOnOutsideClick={true}
        contentRender={content}
        onHiding={() => this.handleTogglePopup(popupName)}
        className={className}
      />
    );
  }
}

UniPopup.propTypes = {
  title: PropTypes.string,
  className: PropTypes.string,
  popupName: PropTypes.string.isRequired,
  content: PropTypes.func.isRequired
};

const mapStateToProps = (state, ownProps) => {
  const popup = state.popups[ownProps.popupName];
  return {
    visible: popup.visible
  };
};

const mapDispatchToProps = dispatch => {
  return {
    togglePopup: popupName => {
      dispatch({ type: "TOGGLE_POPUP", popup: popupName });
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UniPopup);
