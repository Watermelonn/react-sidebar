import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Popup } from "devextreme-react";

class DxPopup extends Component {
  handleTogglePopup = popupName => {
    this.props.togglePopup(popupName);
  };
  
  render() {
    const { visible, title, content, className, popupName } = this.props;

    console.log("visible:",visible);
    console.log("title:",title);
    console.log("content:",content);
    console.log("className:",className);
    console.log("popupName:",popupName);

    return (
      <Popup
        visible={visible}
        showTitle={true}
        title={title}
        dragEnabled={false}
        closeOnOutsideClick={true}
        contentRender={content}
        onHiding={() => this.handleTogglePopup(popupName)}
        className={className}
      />
    );
  }
}

DxPopup.propTypes = {
  title: PropTypes.string,
  className: PropTypes.string,
  popupName: PropTypes.string.isRequired
};

const mapStateToProps = (state, ownProps) => {
  const popup = state.popups[ownProps.popupName];
  return {
    visible: popup.visible
  };
};

const mapDispatchToProps = dispatch => {
  return {
    togglePopup: popupName => {
      dispatch({ type: "TOGGLE_POPUP", popup: popupName });
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DxPopup);
