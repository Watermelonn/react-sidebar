import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Button } from "devextreme-react";
import DxPopup from "./dxPopup";

class OkCancelDialog extends Component {

    constructor(props){
        super(props);
        this.renderDialog = this.renderDialog.bind(this);
    }

  handleCloseDialog = () => {
    this.props.togglePopup("okCancelDialog");
  };

  render() {
    const { title, className, popupName } = this.props;
    return (
      <DxPopup
        title={title}
        className={className}
        popupName={popupName}
        content={this.renderDialog}
      />
    );
  }

  renderDialog() {
    const { message, dangerous, cancelText, okText, onOkClick } = this.props;
    const okButtonType = dangerous ? "danger" : "normal";

    return (
      <React.Fragment>
        <p className={"dialog-message"}>{message}</p>
        <Button
          className={"dialog-button-cancel"}
          type={"normal"}
          width={90}
          text={cancelText}
          onClick={this.handleCloseDialog}
        />
        <Button
          className={"dialog-button-ok"}
          type={okButtonType}
          width={90}
          text={okText}
          onClick={() => onOkClick()}
        />
      </React.Fragment>
    );
  }
}

OkCancelDialog.propTypes = {
  className: PropTypes.string,
  popupName: PropTypes.string,
  title: PropTypes.string,
  message: PropTypes.string,
  dangerous: PropTypes.bool,
  cancelText: PropTypes.string,
  okText: PropTypes.string,
  onOkClick: PropTypes.func
};

const mapStateToProps = state => {
  return {
    
  };
};

const mapDispatchToProps = dispatch => {
  return {
    togglePopup: popupName =>
      dispatch({ type: "TOGGLE_POPUP", popup: popupName })
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OkCancelDialog);
