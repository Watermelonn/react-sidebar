import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Button } from "devextreme-react";
import UniPopup from "./uniPopup";

class OkCancelPopup extends Component {
  constructor(props) {
    super(props);
    this.renderDialog = this.renderDialog.bind(this);
  }

  render() {
    const { title, popupName, className } = this.props;
    return (
      <UniPopup
        title={title}
        className={className || "dialog-ok-cancel"}
        popupName={popupName}
        content={this.renderDialog}
      />
    );
  }

  renderDialog() {
    const {
      message,
      dangerous,
      cancelText,
      okText,
      onOkClick,
      onCancelClick,
      okButtonWidth
    } = this.props;
    const okButtonType = dangerous ? "danger" : "normal";

    return (
      <React.Fragment>
        <p className={"dialog-message"}>{message}</p>
        <Button
          className={"dialog-button-cancel"}
          type={"normal"}
          width={90}
          text={cancelText}
          onClick={() => onCancelClick()}
        />
        <Button
          className={"dialog-button-ok"}
          type={okButtonType}
          width={okButtonWidth || 90}
          text={okText}
          onClick={() => onOkClick()}
        />
      </React.Fragment>
    );
  }
}

OkCancelPopup.propTypes = {
  popupName: PropTypes.string.isRequired,
  title: PropTypes.string,
  message: PropTypes.string,
  dangerous: PropTypes.bool,
  cancelText: PropTypes.string,
  okText: PropTypes.string,
  onOkClick: PropTypes.func,
  onCancelClick: PropTypes.func,
  className: PropTypes.string
};

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    togglePopup: popupName =>
      dispatch({ type: "TOGGLE_POPUP", popup: popupName })
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OkCancelPopup);
