import React from "react";
import DataGrid, {
  Sorting,
  SearchPanel,
  FilterPanel,
  FilterBuilderPopup,
  HeaderFilter,
  Export,
  FilterRow
} from "devextreme-react/data-grid";
import { Scrolling } from "devextreme-react/tree-list";
import PropTypes from "prop-types";

const doNothing = () => {};

const UniTable = ({
  data,
  alternatingRowColours,
  onSelectionChanged,
  onDoubleClick,
  exportName,
  exportEnabled,
  children,
  reference,
  onCellPrepared,
  showBorders,
  showColumnLines,
  columnAutoWidth,
  hideFilterPanel,
  hideHeaderFilter
}) => {
  const filterBuilderPopupPosition = {
    of: window,
    at: "top",
    my: "top",
    offset: { y: 10 }
  };

  return (
    <div className="table-container">
      <DataGrid
        dataSource={data}
        selection={{ mode: "single" }}
        hoverStateEnabled={true}
        showBorders={showBorders == null ? true : showBorders}
        showColumnLines={showColumnLines == null ? true : showColumnLines}
        onSelectionChanged={onSelectionChanged}
        onRowDblClick={onDoubleClick}
        allowColumnResizing={true}
        rowAlternationEnabled={alternatingRowColours}
        height={"100%"}
        ref={reference}
        onCellPrepared={
          onCellPrepared ? e => onCellPrepared(e) : () => doNothing()
        }
        columnAutoWidth={columnAutoWidth}
      >
        <Scrolling mode={"infinite"} />
        <SearchPanel visible={true} width={260} placeholder={"Search..."} />
        <Sorting mode={"multiple"} />
        <FilterPanel visible={!hideFilterPanel} />
        <FilterBuilderPopup position={filterBuilderPopupPosition} />
        <HeaderFilter visible={!hideHeaderFilter} />
        <Export enabled={exportEnabled} fileName={exportName} />
        {hideFilterPanel ? <React.Fragment /> : <FilterRow visible={true} />}
        {children}
      </DataGrid>
    </div>
  );
};

UniTable.propTypes = {
  data: PropTypes.object.isRequired,
  alternatingRowColours: PropTypes.bool,
  onSelectionChanged: PropTypes.func.isRequired,
  onDoubleClick: PropTypes.func.isRequired,
  exportName: PropTypes.string
};

export default UniTable;
