import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Button } from "devextreme-react";

const TableButtonGroup = ({
  buttons,
  heading,
  buttonsDisabled
}) => {
  const length = buttons.length;
  const numberOfRows = Math.ceil(buttons.length / 4);

  let newButtonsArray = [];

  let i = 0;
  while (i <= length) {
    newButtonsArray.push({
      id: i,
      buttons: buttons.slice(i, i + numberOfRows)
    });

    i += numberOfRows;
  }

  return (
    <div className="panel table-button-container pt-3">
      <span className="section-heading">{heading}</span>
      <div className="row pt-2">
        {newButtonsArray.map(({ id, buttons }) => (
          <div key={id}>
            {buttons.map(
              ({
                id,
                width,
                text,
                type,
                hidden,
                onClick,
                onClickParams,
                disabled
              }) =>
                hidden ? (
                  <React.Fragment key={id} />
                ) : (
                  <Button
                    key={id}
                    width={width}
                    text={text}
                    type={type}
                    stylingMode={"contained"}
                    className={"mb-2 ml-2"}
                    onClick={() => onClick(onClickParams)}
                    disabled={buttonsDisabled || disabled}
                  />
                )
            )}
          </div>
        ))}
      </div>
    </div>
  );
};

TableButtonGroup.propTypes = {
  buttons: PropTypes.array.isRequired,
  heading: PropTypes.string,
  type: PropTypes.string.isRequired,
  tableName: PropTypes.string.isRequired
};

const mapStateToProps = (state, ownProps) => {
  const table = { ...state[ownProps.type][ownProps.tableName] };
  return {
    buttonsDisabled: table.buttonsDisabled
  };
};

export default connect(mapStateToProps)(TableButtonGroup);
