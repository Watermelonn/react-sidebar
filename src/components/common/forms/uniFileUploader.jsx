import React, { Component } from "react";
import { FileUploader } from "devextreme-react";

class UniFileUploader extends Component {
  constructor(props) {
    super(props);

    let open = XMLHttpRequest.prototype.open;
    XMLHttpRequest.prototype.open = function() {
      open.apply(this, arguments);
      this.withCredentials = true;
    };
  }

  render() {
    const { name, label, multiple, uploadUrl, onUpload, disabled } = this.props;

    return (
      <div className="form-group mb-0" id={`div${name}`}>
        <label htmlFor={`txt${name}`}>{label}</label>
        <FileUploader
          uploadMode={"instantly"}
          allowedFileExtensions={[".jpg", ".jpeg", ".gif", ".png"]}
          multiple={multiple}
          uploadUrl={`${
            process.env.NODE_ENV === "development"
              ? "http://localhost:3828/api/fileupload/"
              : ""
          }${uploadUrl}`}
          onUploaded={e => onUpload(e)}
          disabled={disabled}
        />
      </div>
    );
  }
}

export default UniFileUploader;
