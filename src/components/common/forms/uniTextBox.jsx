import React, { Component } from "react";
import { TextBox } from "devextreme-react";
import Validator from "devextreme-react/validator";
import PropTypes from "prop-types";

class UniTextBox extends Component {
  render() {
    const {
      label,
      name,
      placeholder,
      onChange,
      readonly,
      value,
      children,
      disabled,
      className
    } = this.props;

    return (
      <div className={`form-group ${className}`} id={`div${name}`}>
        <label htmlFor={`${name}`}>{label}</label>
        <TextBox
          id={`${name}`}
          placeholder={placeholder}
          onKeyUp={e => onChange(e)}
          value={value}
          readOnly={readonly}
          disabled={disabled}
        >
          <Validator>{children}</Validator>
        </TextBox>
      </div>
    );
  }
}

UniTextBox.propTypes = {
  label: PropTypes.string,
  name: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  readonly: PropTypes.bool
};

export default UniTextBox;
