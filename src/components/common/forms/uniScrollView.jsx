import React, { Component } from "react";
import { ScrollView } from "devextreme-react";

class UniScrollView extends Component {
  render() {
    return (
      <div className="scrollViewContainer">
        <ScrollView
          showScrollbar={"onScroll"}
          scrollByContent={true}
          scrollByThumb={true}
        >
          {this.props.children}
        </ScrollView>
      </div>
    );
  }
}

export default UniScrollView;
