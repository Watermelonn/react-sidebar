import React from "react";
import { Button } from "devextreme-react";
import PropTypes from "prop-types";

const SubmitButtonsGroup = ({ onClear, editMode }) => {
  return (
    <React.Fragment>
      {editMode ? (
        <div className="form-group mt-4 pull-right" id="divUpdate">
          <Button
            width={90}
            text={"Update"}
            type={"default"}
            stylingMode={"contained"}
            useSubmitBehavior={true}
          />
        </div>
      ) : (
        <div className="form-group mt-4 pull-right" id="divSubmit">
          <Button
            width={90}
            text={"Clear"}
            type={"normal"}
            stylingMode={"contained"}
            onClick={() => onClear()}
          />
          <Button
            width={90}
            text={"Submit"}
            type={"default"}
            stylingMode={"contained"}
            className={"ml-2"}
            useSubmitBehavior={true}
          />
        </div>
      )}
    </React.Fragment>
  );
};

SubmitButtonsGroup.propTypes = {
  editMode: PropTypes.bool.isRequired
};

export default SubmitButtonsGroup;
