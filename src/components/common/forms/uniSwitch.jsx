import React, { Component } from "react";
import PropTypes from "prop-types";
import { Switch } from "devextreme-react";

class UniSwitch extends Component {
  render() {
    const { name, label, value, onValueChanged } = this.props;

    return (
      <div className="input-group" id={`div${name}`}>
        <label htmlFor={`txt${name}`}>{label}</label>
        <Switch
          id={`txt${name}`}
          value={value}
          onValueChanged={e => onValueChanged(e)}
        />
      </div>
    );
  }
}

UniSwitch.propTypes = {
  name: PropTypes.string,
  label: PropTypes.string,
  value: PropTypes.bool,
  onValueChanged: PropTypes.func
};

export default UniSwitch;
