import React from "react";
import { DateBox } from "devextreme-react";
import Validator from "devextreme-react/validator";
import PropTypes from "prop-types";

const UniDatePicker = ({
  label,
  name,
  disabled,
  disabledDates,
  onChange,
  value,
  placeholder,
  readonly,
  children
}) => {
  return (
    <div className="form-group" id={`div${name}`}>
      <label htmlFor={`${name}`}>{label}</label>
      <div className="input-group">
        <DateBox
          id={`${name}`}
          type={"date"}
          className="full-width"
          openOnFieldClick={true}
          displayFormat="dd/MM/yyyy"
          disabled={disabled}
          disabledDates={args => disabledDates(args)}
          onValueChanged={e => onChange(e)}
          value={value}
          acceptCustomValue={false}
          placeholder={placeholder}
          calendarOptions={{ firstDayOfWeek: 1 }}
          dateSerializationFormat={"yyyy-MM-ddTHH:mm:ss"}
          readOnly={readonly}
        >
          <Validator>{children}</Validator>
        </DateBox>
      </div>
    </div>
  );
};

UniDatePicker.propTypes = {
  label: PropTypes.string,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string,
  placeholder: PropTypes.string,
  disabledDates: PropTypes.func.isRequired
};

export default UniDatePicker;
