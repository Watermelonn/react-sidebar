import React, { Component } from "react";
import PropTypes from "prop-types";
import { SelectBox } from "devextreme-react";
import Validator from "devextreme-react/validator";

class UniSelectBox extends Component {
  render() {
    const {
      label,
      name,
      items,
      disabled,
      dataSource,
      onChange,
      value,
      reference,
      readonly
    } = this.props;

    return (
      <div className="form-group" id={`div${name}`}>
        <label htmlFor={`${name}`}>{label}</label>
        <SelectBox
          id={`${name}`}
          items={items}
          valueExpr={"id"}
          displayExpr={"value"}
          disabled={disabled}
          searchEnabled={!this.props.searchDisabled}
          dataSource={dataSource}
          onValueChanged={e => onChange(e)}
          value={value}
          ref={reference}
          readOnly={readonly}
        >
          <Validator>{this.props.children}</Validator>
        </SelectBox>
      </div>
    );
  }
}

UniSelectBox.propTypes = {
  label: PropTypes.string,
  name: PropTypes.string.isRequired,
  items: PropTypes.array,
  dataSource: PropTypes.object,
  value: PropTypes.number,
  onChange: PropTypes.func.isRequired,
  readonly: PropTypes.bool
};

export default UniSelectBox;
