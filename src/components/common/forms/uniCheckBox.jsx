import React from "react";
import PropTypes from "prop-types";

const UniCheckBox = ({
  label,
  name,
  className,
  onChange,
  value,
  defaultValue,
  args,
  isRadio,
  disabled
}) => {
  return (
    <React.Fragment>
      <label className={`checkbox-container ${className}`}>
        {label}
        <input
          id={name}
          type="checkbox"
          checked={value}
          defaultChecked={defaultValue}
          onChange={e => onChange(e, args)}
          disabled={disabled}
        />
        <span className={`${isRadio ? "radiomark" : "checkmark"}`} />
      </label>
    </React.Fragment>
  );
};

UniCheckBox.propTypes = {
  label: PropTypes.string,
  name: PropTypes.string,
  className: PropTypes.string,
  onChange: PropTypes.func,
  value: PropTypes.bool,
  defaultValue: PropTypes.bool
};

export default UniCheckBox;
