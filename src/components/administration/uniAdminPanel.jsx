import React, { Component } from "react";
import { connect } from "react-redux";
import { Button } from "devextreme-react";
import PropTypes from "prop-types";
import { faPlus, faTrash } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import OkCancelPopup from "../common/popups/okCancelPopup";
import UniTable from "../common/tables/uniTable";
import UniPopup from "../common/popups/uniPopup";
import AddRecord from "./partials/addRecord";

class UniAdminPanel extends Component {
  constructor(props) {
    super(props);
    this.datagrid = React.createRef();
  }

  handleSelectionChanged = ({ selectedRowsData }) => {
    const { enableAdminTableButtons, setAdminSelectedId, type } = this.props;
    const data = { ...selectedRowsData[0] };
    enableAdminTableButtons(type);
    setAdminSelectedId(data.id, type);
    this.forceUpdate();
  };

  handleShowDeleteDialog = () => {
    const { togglePopup, type } = this.props;
    togglePopup(`${type}DeletePopup`);
  };

  handleShowAddPopup = () => {
    const { togglePopup, type } = this.props;
    togglePopup(`${type}Popup`);
  };

  handleDelete = async () => {
  };

  handleAdd = () => {
    this.datagrid.current.instance.refresh();
  };

  doNothing = () => {};

  shouldComponentUpdate() {
    return false;
  }

  render() {
    const {
      label,
      dataSource,
      type,
      buttonsDisabled,
      icon,
      colour
    } = this.props;

    return (
      <div
        className={`col-lg-4 mt-3 mb-3 administration-panel-container ${type}-container`}
      >
        <div className={"panel"}>
          <div className={"administration-label"}>
            <FontAwesomeIcon
              icon={icon}
              className={"mr-2 administration-icon"}
              style={{ backgroundColor: `${colour}` }}
            />
            <span>{`${label}s`}</span>
          </div>
          <UniTable
            data={dataSource}
            alternatingRowColours={true}
            onDoubleClick={this.doNothing}
            onSelectionChanged={e => this.handleSelectionChanged(e)}
            reference={this.datagrid}
            showBorders={true}
            showColumnLines={true}
            columnAutoWidth={true}
            hideFilterPanel={true}
          />
          <div className={"col-md-12 p-0 pt-3"}>
            <Button
              className={"administration-add-button"}
              width={85}
              text={"Add"}
              type={"normal"}
              stylingMode={"outlined"}
              onClick={() => this.handleShowAddPopup()}
              render={buttonData => (
                <React.Fragment>
                  <span className={"dx-button-text"}>
                    <FontAwesomeIcon icon={faPlus} className={"mr-2"} />
                    {buttonData.text}
                  </span>
                </React.Fragment>
              )}
            />
            <Button
              width={100}
              text={"Delete"}
              type={"normal"}
              stylingMode={"outlined"}
              className={"mb-2 pull-right"}
              onClick={() => this.handleShowDeleteDialog()}
              disabled={buttonsDisabled}
              render={buttonData => (
                <React.Fragment>
                  <span className={"dx-button-text"}>
                    <FontAwesomeIcon icon={faTrash} className={"mr-2"} />
                    {buttonData.text}
                  </span>
                </React.Fragment>
              )}
            />
          </div>
        </div>
        <OkCancelPopup
          title={`Delete ${label}`}
          message={`Are you sure want to delete this ${label}?`}
          popupName={`${type}DeletePopup`}
          dangerous={true}
          cancelText={"Cancel"}
          okText={"Delete"}
          onOkClick={this.handleDelete}
          onCancelClick={this.handleShowDeleteDialog}
        />
        <UniPopup
          title={`Add ${label}`}
          className={"popup-add-record"}
          content={this.renderPopup}
          popupName={`${type}Popup`}
        />
      </div>
    );
  }

  renderPopup = () => {
    const { type, label } = this.props;
    return <AddRecord type={type} label={label} onSuccess={this.handleAdd} />;
  };
}

UniAdminPanel.propTypes = {
  label: PropTypes.string,
  dataSource: PropTypes.object.isRequired,
  type: PropTypes.string.isRequired
};

const mapStateToProps = (state, ownProps) => {
  const { administration } = state;
  const { selectedId, buttonsDisabled, refresh } = administration[
    ownProps.type
  ];
  return {
    selectedId,
    buttonsDisabled,
    refresh
  };
};

const mapDispatchToProps = dispatch => {
  return {
    enableAdminTableButtons: name => {
      dispatch({ type: "ENABLE_ADMIN_TABLE_BUTTONS", name });
    },
    setAdminSelectedId: (id, name) => {
      dispatch({ type: "SET_ADMIN_SELECTED_ID", id, name });
    },
    togglePopup: popup => {
      dispatch({ type: "TOGGLE_POPUP", popup });
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UniAdminPanel);
