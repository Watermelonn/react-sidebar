import React, { Component } from "react";
import { connect } from "react-redux";
import UniScrollView from "../common/forms/uniScrollView";
import UniAdminPanel from "./uniAdminPanel";
import {
  faHardHat,
  faBuilding,
  faShieldAlt,
  faClock,
  faLayerGroup,
  faPen
} from "@fortawesome/free-solid-svg-icons";

class ControlPanel extends Component {
  componentDidMount() {
    this.props.setPageTitle("Control Panel");
  }

  render() {
    const tradeDataSource = {
    
    };

    const coreDisciplineDataSource = {
    
    };

    const departmentDataSource = {
      
    };

    const screeningTypeDataSource = {
     
    };

    const contractorTypeDataSource = {
     
    };

    const authorisorDataSource = {
      
    };

    const adminComponents = [
      {
        id: 1,
        label: "Trade",
        dataSource: tradeDataSource,
        type: "trade",
        icon: faHardHat,
        colour: "#ffc720"
      },
      {
        id: 2,
        label: "Core Discipline",
        dataSource: coreDisciplineDataSource,
        type: "coreDiscipline",
        icon: faLayerGroup,
        colour: "#eb3573"
      },
      {
        id: 3,
        label: "Department",
        dataSource: departmentDataSource,
        type: "department",
        icon: faBuilding,
        colour: "#009688"
      },
      {
        id: 4,
        label: "Screening Type",
        dataSource: screeningTypeDataSource,
        type: "screeningType",
        icon: faShieldAlt,
        colour: "#1db2f5"
      },
      {
        id: 5,
        label: "Contractor Type",
        dataSource: contractorTypeDataSource,
        type: "contractorType",
        icon: faClock,
        colour: "#f5564a"
      },
      {
        id: 6,
        label: "Authorisor",
        dataSource: authorisorDataSource,
        type: "authorisor",
        icon: faPen,
        colour: "#97c95c"
      }
    ];

    return (
      <div className={"container-fluid"}>
        <div className={"row pt-0"}>
          {adminComponents.map(
            ({ id, onDelete, label, dataSource, type, icon, colour }) => (
              <UniAdminPanel
                key={id}
                onDelete={onDelete}
                label={label}
                dataSource={dataSource}
                type={type}
                icon={icon}
                colour={colour}
              />
            )
          )}
        </div>
      </div>
    );
  }

  renderPopup = content => {
    return <UniScrollView>{content}</UniScrollView>;
  };
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    togglePopup: popupName => {
      dispatch({ type: "TOGGLE_POPUP", popup: popupName });
    },
    setPageTitle: title => {
      dispatch({ type: "SET_PAGE_TITLE", title });
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ControlPanel);
