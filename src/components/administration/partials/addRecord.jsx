import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import UniTextBox from "../../common/forms/uniTextBox";
import { Button } from "devextreme-react";
class AddRecord extends Component {
  handleAddRecord = async () => {

  };

  handleRecordChanged = async e => {

  };

  handleChange = e => {
    const { changeInputValue, type } = this.props;
    const inputName = e.element.id;
    const value = e.event.target.value;

    changeInputValue(type, inputName, value);
  };

  render() {
    const { descriptionExists, description, label } = this.props;
    return (
      <React.Fragment>
        <form value={label}>
          <div className={"row pl-3 pr-3 pt-1"}>
            <div className={"col-md-12"}>
              <UniTextBox
                label={`${label} Name`}
                name={"description"}
                placeholder={`${label} Name`}
                onChange={this.handleRecordChanged}
                value={description}
              />
              <span className="add-record-confirmation">
                {descriptionExists ? `This ${label} already exists.` : ""}
              </span>
            </div>
          </div>
          <div className={"pr-3 pt-3"}>
            <Button
              width={90}
              text={"Add"}
              type={"default"}
              stylingMode={"contained"}
              className={"ml-2 pull-right"}
              value={"Record"}
              onClick={this.handleAddRecord}
              disabled={descriptionExists}
            />
          </div>
        </form>
      </React.Fragment>
    );
  }
}

AddRecord.propTypes = {
  type: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  onSuccess: PropTypes.func.isRequired
};

const mapStateToProps = (state, ownProps) => {
  const { administration } = state;
  return {
    description: administration[ownProps.type].description,
    descriptionExists: administration[ownProps.type].descriptionExists
  };
};

const mapDispatchToProps = dispatch => {
  return {
    changeInputValue: (name, inputName, value) => {
      dispatch({ type: "CHANGE_ADMIN_INPUT_VALUE", name, inputName, value });
    },
    togglePopup: popupName => {
      dispatch({ type: "TOGGLE_POPUP", popup: popupName });
    },
    clearAdminInputValues: name => {
      dispatch({ type: "CLEAR_ADMIN_INPUT_VALUES", name });
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddRecord);
