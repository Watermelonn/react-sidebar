const contractorReports = [
  {
    id: 9,
    text: "Test 9",
    tooltip: "Test 9"
  },
  {
    id: 10,
    text: "Test 10",
    tooltip: "Test 10"
  },
  {
    id: 11,
    text: "Test 11",
    tooltip: "Test 11"
  },
  {
    id: 12,
    text: "Test 12",
    tooltip: "Test 12"
  },
  {
    id: 13,
    text: "Test 13",
    tooltip: "Test 13"
  },
  {
    id: 14,
    text: "Test 14",
    tooltip: "Test 14"
  },
  {
    id: 15,
    text: "Test 15",
    tooltip: "Test 15"
  },
  {
    id: 16,
    text: "Test 16",
    tooltip: "Test 16"
  }
];

export default contractorReports;
