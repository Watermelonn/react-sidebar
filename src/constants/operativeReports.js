const operativeReports = [
  {
    id: 1,
    text: "Test 1",
    tooltip: "Test 1"
  },
  {
    id: 2,
    text: "Test 2",
    tooltip: "Test 2"
  },
  {
    id: 3,
    text: "Test 3",
    tooltip: "Test 3"
  },
  {
    id: 4,
    text: "Test 4",
    tooltip: "Test 4"
  },
  {
    id: 5,
    text: "Test 5",
    tooltip: "Test 5"
  },
  {
    id: 6,
    text: "Test 6",
    tooltip: "Test 6"
  },
  {
    id: 7,
    text: "Test 7",
    tooltip: "Test 7"
  },
  {
    id: 8,
    text: "Test 8",
    tooltip: "Test 8"
  }
];

export default operativeReports;
