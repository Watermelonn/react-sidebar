import React from "react";
import AddEditOperative from "../components/operatives/addEditOperative";

const popups = [
  {
    id: 1,
    title: "Edit Operative",
    className: "popup-edit-operative",
    content: <AddEditOperative editMode={true} />,
    popupName: "editOperativePopup"
  },
  {
    id: 3,
    title: "Operative Notes",
    className: "popup-notes",
    content: <div />,
    popupName: "notesPopup"
  },
  {
    id: 4,
    title: "View Swipes",
    className: "popup-view-swipes",
    content: <div />,
    popupName: "viewSwipesPopup"
  },
  {
    id: 5,
    title: "Manual Swipe",
    className: "popup-manual-swipe",
    content: <div />,
    popupName: "manualSwipePopup"
  },
  {
    id: 6,
    title: "Edit Pass Number",
    className: "popup-edit-pass-number",
    content: <div />,
    popupName: "editPassNumberPopup"
  },
  {
    id: 7,
    title: "Manage Access",
    className: "popup-manage-access",
    content: <div />,
    popupName: "manageAccessPopup"
  },
  {
    id: 8,
    title: "Print Card",
    className: "popup-print-card",
    content: <div />,
    popupName: "printCardPopup"
  },
  {
    id: 9,
    title: "Weekend Work Request",
    className: "popup-weekend-work",
    content: <div />,
    popupName: "weekendWorkPopup"
  }
];

export default popups;
