import {
  faUsers,
  faHardHat,
  faChartPie,
  faUsersCog,
  faBook
} from "@fortawesome/free-solid-svg-icons";

const sidebarSecondaryItems = [
  {
    id: "1",
    name: "Documentation",
    icon: faBook,
    href: "#",
    style: {
      color: "#e91e63"
    }
  }
];

const sidebarPrimaryItems = [
  {
    id: "1",
    name: "Operatives",
    icon: faUsers,
    style: {
      color: "#eb3573"
    },
    closed: false,
    allHover: false,
    mainHover: false,
    subHover: false,
    href: "#",

    subList: [
      {
        id: "1_1",
        name: "Add New",
        href: "/operatives/addNew?editMode=false"
      },
      {
        id: "1_2",
        name: "Manage",
        href: "/operatives/manage"
      }
    ]
  },
  {
    id: "2",
    name: "Companies",
    icon: faHardHat,
    style: {
      color: "#ffc720"
    },
    closed: true,
    allHover: false,
    mainHover: false,
    subHover: false,
    href: "#",

    subList: [
      {
        id: "2_1",
        name: "Manage",
        href: "/contractors/manage"
      }
    ]
  },
  {
    id: "3",
    name: "Reports",
    icon: faChartPie,
    style: {
      color: "#1db2f5"
    },
    href: "/reports/menu"
  },
  {
    id: "5",
    name: "Administration",
    icon: faUsersCog,
    style: {
      color: "#f5564a"
    },
    closed: true,
    allHover: false,
    mainHover: false,
    subHover: false,

    subList: [
      {
        id: "4_1",
        name: "Control Panel",
        href: "/administration/controlPanel"
      }
    ]
  }
];

export { sidebarPrimaryItems, sidebarSecondaryItems };
