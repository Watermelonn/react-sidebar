const BundleAnalyzerPlugin = require("webpack-bundle-analyzer")
  .BundleAnalyzerPlugin;

module.exports = function override(config, env) {
  if (!config.plugins) config.plugins = [];
  if (process.env.NODE_ENV !== "production") return config;

  // config.plugins.push(new BundleAnalyzerPlugin());
  return config;
};
